﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class SimpleBinaryModuleImpl : MonoBehaviour
{
	
	public enum VIEW_MODE { INVISIBLE, FT2D, HSKL, CAPE, ALL, GRASP, SD, MARKER_UI }
	public enum UI_MODE { NONE, HAND, MARKER }
	const int RESET_FRAME = 30; // 30/60 [sec] = 0.5 second
	
	const string NATIVE_DLL_NAME = "Object Alive_Online";
	
	float[] armarker_pos = new float[3];
	float[] armarker_rotation = new float[4];
	public GameObject arcursor;
	#region NATIVE_FUNTIONS
	
	// class RGBDCamera
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void initBinaryModule_SD(float hand_width, float hand_length);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void updateBinaryModule(bool handTracking);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void endBinaryModule_SD();
	
	// getter for raw-depth (point cloud / mesh)
	[DllImport(NATIVE_DLL_NAME)]
	private static extern IntPtr getPointCloud(out int size);
	
	// getters for HSKL
	[DllImport(NATIVE_DLL_NAME)]
	private static extern float getHandError();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getHandMeshVertices(float[] hand_verts);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getBoneModelMatrices(float[] hskl_poses);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void resetHandSize(float hand_w, float hand_h);
	
	// getters for fingertips
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFingerTipsFromHSKL(float[] pos_list);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFingerTipsFrom2D(float[] pos_list);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getPalmPos_DT(float[] pos);
	
	// getters for CAPE
	[DllImport(NATIVE_DLL_NAME)]
	private static extern bool getAction();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFT2DTip(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getHSKLTip(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getTAPETip(float[] pos);
	
	// ARToolkit modules
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int initMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int runMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void endMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getMarkerPos(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getMarkerRotation(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int[] GetMarkerNum_test();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int GetRegisteredMarkerNum_test();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int GetRegisteredMarkerID_test(int num);
	#endregion // NATIVE_FUNTIONS
	
	
	
	
	public int num_of_marker { get; set; }
	
	
	void Start()
	{
		float HAND_WIDTH = 0.08f;
		
		float HAND_LENGTH = 0.19f;
		initBinaryModule_SD(HAND_WIDTH, HAND_LENGTH);
		/*
		InitMarker(); // AR Marker
		
		int num_regidit = GetRegisteredMarkerNum_test ();
		Debug.Log (num_regidit);
		
		for(int i=0; i<num_regidit; i++)
			Debug.Log (GetRegisteredMarkerID_test(i));
		*/
	}
	
	void Update()
	{
		//UpdateByLiveFrame();
	}
	public void UpdateByLiveFrame()
	{
		// update live camera
		updateBinaryModule(true);
		
		
		//		bool action = getAction ();
		//		Debug.Log (action);
		
		
		num_of_marker = runMarkerTracking();
		//Debug.Log (num_of_marker);
		getMarkerPos(armarker_pos);
		getMarkerRotation(armarker_rotation);
		ManipulateMarker();
		/*
        int[] markerNum = GetMarkerNum_test();
		Debug.Log ("##");
		for (int i=0; i<num_of_marker; i++) 
		{
			Debug.Log(markerNum[i]);
		}*/
		
		//		Debug.Log ("##");
		
		
	}
	
	
	public void OnDestroy()
	{
		//endMarkerTracking();
		endBinaryModule_SD();
		//mSerialConnector.OnDestroy();
	}
	
	
	
	
	
	#region PRIVATE_METHODS_FOR_ARTOOLKIT
	
	private void InitMarker()
	{
		initMarkerTracking();
		//		Debug.Log ("result" + result.ToString ());
		
		// AR cursor
		//arcursor = GameObject.Find("ARWand");//GameObject.CreatePrimitive(PrimitiveType.Cube);
		
		arcursor = GameObject.Instantiate(Resources.Load("ARWand") as GameObject, transform.position, transform.rotation) as GameObject;
		//arcursor = GameObject.CreatePrimitive(PrimitiveType.Cube);
		arcursor.name = "ARCursor";
		//arcursor.transform.parent = hierarchy.transform;
		// arcursor.transform.position = hierarchy.transform.position;
		//arcursor.transform.rotation = hierarchy.transform.rotation;
		//arcursor.transform.localScale = new Vector3(86, 86, 86);
		
		//arcursor.renderer.renderer.castShadows = false;
		//arcursor.collider.enabled = false;
		
		// get texture from resource folder
		//cursor_red = Resources.Load("cursor_red") as Texture2D;
		//cursor_blue = Resources.Load("cursor_blue") as Texture2D;
		//arcursor.renderer.material.shader = Shader.Find("Unlit/Transparent");
		//arcursor.renderer.material.mainTexture = cursor_blue;
	}
	
	private void ManipulateMarker()
	{
		//if (num_of_marker > 0)
		{
			//const float near = 1.0f;
			
			float x = armarker_pos[0];
			float y = armarker_pos[1];
			float z = armarker_pos[2];
			
			//float X = -near * x / z;
			//float Y = -near * y / z;
			
			Vector3 pos = new Vector3(x, y, z);
			//Debug.Log(pos);
			arcursor.transform.position = pos;
			arcursor.transform.localRotation = new Quaternion(armarker_rotation[0], armarker_rotation[1], armarker_rotation[2], armarker_rotation[3]);
			//arcursor.transform.Rotate(Vector3.right, 180, Space.World);
			//arcursor.transform.Rotate(Vector3.up, 180);
			//arcursor.transform.Rotate(Vector3.forward, 180, Space.World);
			//arcursor.transform.Rotate(Vector3.up, 180);
			// set visible
			//arcursor.renderer.enabled = true;
			/*
			if (num_of_marker == 1) arcursor.renderer.material.mainTexture = cursor_red;
			else arcursor.renderer.material.mainTexture = cursor_blue;
             */
		}
		//else
		{
			// set invisible
			//arcursor.renderer.enabled = false;
		}
	}
	
	#endregion // PRIVATE_METHODS_FOR_ARTOOLKIT
	
	
	
	
}
