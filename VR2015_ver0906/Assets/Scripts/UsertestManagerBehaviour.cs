﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UsertestManagerBehaviour : MonoBehaviour
{
	#region CONSTANTS

	const int GO_NEXT_TASK_FRAME = 15; // 

	char[] delim = { ' ', ',', '(', ')' };

	int[] area_id = { 4, 2, 7, 0, 5, 6, 1, 8, 3,
						7, 2, 3, 8, 1, 6, 5, 0, 4};

	/*
    int[] Seed0 = { 1705344613, -211778561, 35389034, 868236982, -44553987, -197445943, -159514110, 309046071, -1362598329, 621649663, 16945814, -1426292916, -94923211, 24066726, -50958784, 678386456, 837639057, 2108357736 };
    int[] Seed1 = { -871593717, -39358033, -2058061674, 1172585433, 1837676223, 1455384344, -259447407, 341303978, -1128102959, -1855413002, -14603594, -2086839327, -1419146168, -9252153, 1920780376, -168035746, -5485277, 364249825 };
    int[] Seed2 = { -17610404, 834622, -70828, -11034, -1389362, -430121, -1539463, 1733322, -891556, 1252407, -2094339, -415945, 1930644, 1676310, 1024584, -267770, -1352875, 1612897 };
    int[] Seed3 = { 1243823397, 1378509006, -2033676253, 705957211, -116537559, 2023814358, 984296041, 819959474, 670841615, -670732958, 991886441, 2000193470, 828046869, 1112013601, 251046580, 1036044707, 1527521880, -1502653205 };
    int[] Seed4 = { 20, 45, 18, 15, 39, 4, 44, 55, 35, 6 };
    */

      int[] Seed0 = { 1705344613, -211778561, 35389034, 868236982, -44553987, -197445943 };
      int[] Seed1 = { -871593717, -39358033, -2058061674, 1172585433, 1837676223, 1455384344 };
      int[] Seed2 = { -17610404, 834622, -70828, -11034, -1389362, -430121 };
      int[] Seed3 = { 1243823397, 1378509006, -2033676253, 705957211, -116537559, 2023814358 };
	  int[] Seed4= { 20, 45, 18, 15, 39, 4, 44, 55, 35, 6 }; // what for??
      int[] Seed5 = { 1705344613, -211778561, 35389034, 868236982, -44553987, -197445943, -159514110, 309046071, -1362598329/*, 621649663, 16945814, -1426292916, -94923211, 24066726, -50958784, 678386456, 837639057, 2108357736 */};
// 
//     int[] Seed0 = { 1705344613, -211778561, 35389034 };
//     int[] Seed1 = { -871593717, -39358033, -2058061674 };
//     int[] Seed2 = { -17610404, 834622, -70828 };
//     int[] Seed3 = { 1243823397, 1378509006, -2033676253, 705957211, -116537559, 2023814358 };

/*    int[] Seed4 = { 20, 45, 18, 15, 39, 4 };*/


    
	int[][] Seed = new int[9][];

	enum USERTEST_MODE { IDLE, RECORDING, REPLAYING, FREE_PLAY, ANALYSIS };

    [SerializeField]
    string root_directory = "d:/VR2016_experiment/";

	[SerializeField]
	string participant_name = "Participant";

	[SerializeField]
	string folder_name;
  

	[SerializeField]
	float ERROR_BOUND_POS = 20.0f;
	[SerializeField]
	float ERROR_BOUND_ROT = 5.0f;

	[SerializeField]
	float HAND_WIDTH = 0.08f;

	[SerializeField]
	float HAND_LENGTH = 0.19f;


    [SerializeField]
    bool automaticallyRecording = true;

	#endregion // CONSTANTS
	


	#region PRIVATE_MEMBERS

	// objects
	private Camera mOculusL = null;
	private Camera mOculusR = null;
	private ObjectManagerBehaviour mObjectManager;
	private BinaryModuleImpl mBinaryModuleImpl;
    
	// usertest mode
	private USERTEST_MODE test_mode;

	// frame number for recording & replaying
	private int frame_num_now;
	private int frame_num_max;
	
	// scenario number
	private int scenario_num;
    
	//method number
	private int method_num;

    //session number
	private int session_num;

    //object size setting.
    private int objSize_num = 0;
  
	private int task_num;

	private bool go_next_task = false;
	private int remain_frame = 0;

	// for logging
	private ArrayList user_log = new ArrayList();
	private ArrayList[] task_log = new ArrayList[4];
    private bool[] isDone_Scneario = new bool[38];

	// counters for logging

    //for folder naming
    string m_date, m_time;
    string recording_directory;

	#endregion // PRIVATE_MEMBERS



	#region MONO_BEHAVIOUR

	void Start()
	{
		// get Oculus Camera reference
		Camera[] cameras = GameObject.FindObjectsOfType<Camera>();
		for (int i = 0; i < cameras.Length; i++)
		{
			if (cameras[i].name == "CameraLeft" ) mOculusL = cameras[i] as Camera;
			if (cameras[i].name == "CameraRight") mOculusR = cameras[i] as Camera;
		}

        //get Environment Space

		// get ObjectManager for usertest
		mObjectManager = FindObjectOfType<ObjectManagerBehaviour>();

		// constructor of binary module
        mBinaryModuleImpl = new BinaryModuleImpl(HAND_WIDTH, HAND_LENGTH);


		// start as a idle mode
		test_mode = USERTEST_MODE.IDLE;

		// assign random seed into the same array
		Seed[0] = Seed0;
		Seed[1] = Seed1;
		Seed[2] = Seed2;
        Seed[3] = Seed3;
        Seed[4] = Seed4;
        Seed[5] = Seed5;

		// task log
		task_log[0] = new ArrayList();
		task_log[1] = new ArrayList();
		task_log[2] = new ArrayList();
        task_log[3] = new ArrayList();

		// counter

		session_num = 1;
		scenario_num = 0;
		method_num = 0;
		objSize_num = 0;

        mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.HAND;
        mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.GRAB;
		mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_LARGE, session_num);
        ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_LARGE;
        mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);

        // set folder name to contain data
        m_date = System.DateTime.Now.ToString("yyMMdd");
        m_time = System.DateTime.Now.ToString("hhmmss");

	}

	void OnDestroy()
	{
		// destructor of binary module
		mBinaryModuleImpl.OnDestroy();
	}

    void OnGUI()
    {
        float unit = 10;
        float width = unit;
        float height = unit;

        //GUI.skin.box.normal.background = texture;

        int startX_grab = 10;
        int startY_cape = startX_grab + startX_grab + 15 * 4;
        int startY_SD = startX_grab + startY_cape + 15 * 4;

        for (int oi = 0; oi < 3; oi++)
        {
            for (int si = 0; si < 4; si++)
            {
                if(isDone_Scneario[oi*4 + si])
                    GUIDrawRect(new Rect(startX_grab + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.cyan);
                else
                    GUIDrawRect(new Rect(startX_grab + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.gray);
            }
        }

        for (int oi = 0; oi < 3; oi++)
        {
            for (int si = 0; si < 4; si++)
            {
                if (isDone_Scneario[12 + oi * 4 + si])
                    GUIDrawRect(new Rect(startY_cape + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.cyan);
                else
                    GUIDrawRect(new Rect(startY_cape + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.gray);
            }
        }

        for (int oi = 0; oi < 3; oi++)
        {
            for (int si = 0; si < 4; si++)
            {
                if (isDone_Scneario[24 + oi * 4 + si])
                    GUIDrawRect(new Rect(startY_SD + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.cyan);
                else
                    GUIDrawRect(new Rect(startY_SD + si * 1.5f * unit, 10 + oi * 1.5f * unit, width, height), Color.gray);
            }
        }

        //sd
        if(isDone_Scneario[36])
            GUIDrawRect(new Rect(startX_grab + 0 * 1.5f * unit, 10 + 4 * 1.5f * unit, width, height), Color.cyan);
        else
            GUIDrawRect(new Rect(startX_grab + 0 * 1.5f * unit, 10 + 4 * 1.5f * unit, width, height), Color.gray);

        //armarker
        if (isDone_Scneario[37])
            GUIDrawRect(new Rect(startY_SD + 0 * 1.5f * unit, 10 + 4 * 1.5f * unit, width, height), Color.cyan);
        else
            GUIDrawRect(new Rect(startY_SD + 0 * 1.5f * unit, 10 + 4 * 1.5f * unit, width, height), Color.gray);

    }

    private static Texture2D _staticRectTexture;
    private static GUIStyle _staticRectStyle;

    // Note that this function is only meant to be called from OnGUI() functions.
    public static void GUIDrawRect(Rect position, Color color)
    {
        if (_staticRectTexture == null)
        {
            _staticRectTexture = new Texture2D(1, 1);
        }

        if (_staticRectStyle == null)
        {
            _staticRectStyle = new GUIStyle();
        }

        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);


    }

    void FixedUpdate() // there is no physics, so I use "FixedUpdate" here as a log recorder / player
	{

        if (test_mode == USERTEST_MODE.ANALYSIS) return;

        //if (test_mode != USERTEST_MODE.REPLAYING) 
        //    mBinaryModuleImpl.UpdateARmarkerActionState();  

        switch (test_mode)
        {
            case USERTEST_MODE.FREE_PLAY: CheckObjectSelection(); break;
            case USERTEST_MODE.RECORDING: PushScenarioRecording(); break;
			case USERTEST_MODE.REPLAYING:
			if (frame_num_now >= frame_num_max)
			{
				Debug.Log("Playing: reach to end of the recording");
				EndScenarioReplaying();
			}
			else PopScenarioReplaying();
			break;
		case USERTEST_MODE.IDLE: break;
        }

		mBinaryModuleImpl.FixedUpdate();

		// increment frame number
		frame_num_now++;

		////////////////////////////////////
		// lazy update for going to next
		////////////////////////////////////
		if (go_next_task)
		{
			remain_frame--;
			if (remain_frame == 0)
			{
				go_next_task = false;
                //prev_frame_IsMatching = false;
				if (!GoNextTask())
				{
					switch (test_mode)
					{
						case USERTEST_MODE.FREE_PLAY: EndFreePlay(); break;
						case USERTEST_MODE.RECORDING: EndScenarioRecording(); break;
						case USERTEST_MODE.REPLAYING: EndScenarioReplaying(); break;
						default: break;
					}
				}
			}
		}
	}

    void Update()
    {
 
        if (test_mode == USERTEST_MODE.ANALYSIS) return;
        if (test_mode != USERTEST_MODE.REPLAYING) mBinaryModuleImpl.UpdateByLiveFrame();

        if (Input.anyKeyDown)
		{
			////////////////////////////////////////////////////////////
			// reset the HSKL length
			////////////////////////////////////////////////////////////
			if (Input.GetKeyDown(KeyCode.R)) mBinaryModuleImpl.ResetHSKLModel(HAND_WIDTH, HAND_LENGTH);

			////////////////////////////////////////////////////////////
			// set view mode:
			////////////////////////////////////////////////////////////

			if (Input.GetKeyDown(KeyCode.N))
			{
				mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.NONE;
				mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.INVISIBLE;
				mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
			}

			////////////////////////////////////////////////////////////
			// set visibility of CAPE 
			////////////////////////////////////////////////////////////
			//if (Input.GetKeyDown(KeyCode.I)) mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.INVISIBLE;
            //if (Input.GetKeyDown(KeyCode.F)) mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.FT2D;
            //if (Input.GetKeyDown(KeyCode.H)) mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.HSKL;

			if (Input.GetKeyDown(KeyCode.G))
			{
                if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.SD)
                {
                    BinaryModuleImpl.endBinaryModule_SD();
                    BinaryModuleImpl.initBinaryModule(HAND_WIDTH, HAND_LENGTH);
                }

                if (session_num != 1)
                    Debug.Log("error: can't set this method in session " + session_num);

                else
                {
                    mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.HAND;
                    mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.GRAB;
					mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
                    method_num = 0;
                }
			}
			if (Input.GetKeyDown(KeyCode.T))
            {
                if (session_num != 1)
                    Debug.Log("error: can't set this method in session " + session_num);

                else
                {
					if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.SD)
					{
						BinaryModuleImpl.endBinaryModule_SD();
						BinaryModuleImpl.initBinaryModule(HAND_WIDTH, HAND_LENGTH);
					}

					mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.HAND;
                    mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.CAPE;
					mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
                    method_num = 1;
                }
               
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                
                if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.CAPE ||
                    mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.GRAB ||
                    mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.MARKER_UI)
                {

                    BinaryModuleImpl.endBinaryModule();
                   // BinaryModuleImpl.endMarkerTracking();

                    BinaryModuleImpl.initBinaryModule_SD(HAND_WIDTH, HAND_LENGTH);
                }
                   
               mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.HAND;
               mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.SD;
				mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
			   method_num = 2;
            }

            if (Input.GetKeyDown(KeyCode.M))
            {
                if (session_num != 2)
                    Debug.Log("error: can't set this method in session " + session_num);

                else
                {

					if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.SD)
					{
						BinaryModuleImpl.endBinaryModule_SD();
						BinaryModuleImpl.initBinaryModule(HAND_WIDTH, HAND_LENGTH);
					}

					mBinaryModuleImpl.ui_mode = BinaryModuleImpl.UI_MODE.MARKER;
                    mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.MARKER_UI;
					mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
                    method_num = 3;
                }
			}

			////////////////////////////////////////////////////////////
			// free-play session :
			////////////////////////////////////////////////////////////
			if (Input.GetKeyDown(KeyCode.F8)) BeginTraining();
			if (Input.GetKeyDown(KeyCode.F9)) BeginFreePlay(false);
			if (Input.GetKeyDown(KeyCode.F10)) BeginFreePlay(true);

			////////////////////////////////////////////////////////////
			// user test scenario:
			////////////////////////////////////////////////////////////

            //contact selection method
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if( (session_num == 1 && (method_num == 3)) ||
                    (session_num == 2 && (method_num == 0 || method_num == 1)) 
                    )
                    Debug.Log("error: can't set this method in session " + session_num);
                else
                    BeginScenarioRecording(1, mBinaryModuleImpl.view_mode);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2)) BeginScenarioRecording(2, mBinaryModuleImpl.view_mode);
            if (Input.GetKeyDown(KeyCode.Alpha3)) BeginScenarioRecording(3, mBinaryModuleImpl.view_mode);
            if (Input.GetKeyDown(KeyCode.Alpha4)) BeginScenarioRecording(4, mBinaryModuleImpl.view_mode);

			////////////////////////////////////////////////////////////
			// replaying : 27
			////////////////////////////////////////////////////////////
			if (Input.GetKeyDown(KeyCode.Alpha5))
			{
				if (!GoNextTask()) Debug.Log("error: cannot proceed the task, please abort by F12.");
			}

            // cube size change.
         
            if (Input.GetKeyDown(KeyCode.Z))
            {
                if (session_num != 1)
                    Debug.Log("error: can't resize object in session " + session_num);
                else
                {
                    objSize_num = 0;
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_LARGE, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_LARGE;
                }

            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                if (session_num != 1)
                    Debug.Log("error: can't resize object in session " + session_num);

                else
                {
                    objSize_num = 1;
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_MEDIUM, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_MEDIUM;
                }
            }
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (session_num != 1)
                    Debug.Log("error: can't resize object in session " + session_num);

                else
                {
                    objSize_num = 2;
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_SMALL, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_SMALL;
                }
            }
            // session change
            if(Input.GetKeyDown(KeyCode.F1))
            {
                if (test_mode != USERTEST_MODE.IDLE) 
                    Debug.Log("error: cannot proceed the task, please abort by F12.");
                else
                {
                    session_num = 1;
                    objSize_num = 0;
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_LARGE, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_LARGE;
                }
            }
            if(Input.GetKeyDown(KeyCode.F2))
            {
                if (test_mode != USERTEST_MODE.IDLE) 
                    Debug.Log("error: cannot proceed the task, please abort by F12.");
                else
                {
                    session_num = 2;
                    objSize_num = 2; // set small size;
					mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_MEDIUM, session_num);
                    ERROR_BOUND_POS = 25;
                    ERROR_BOUND_ROT = 30;
                }
            }
			if (Input.GetKeyDown(KeyCode.F11)) 
				BeginScenarioReplaying();
			
			if (Input.GetKeyDown(KeyCode.Escape))
				GetTaskLogWithoutReplaying(); // ************************************************************************************************************************ Temporary
            ////////////////////////////////////////////////////////////
            // abort :
            ////////////////////////////////////////////////////////////
            else if (Input.GetKeyDown(KeyCode.F12))
            {
                switch (test_mode)
                {
                    case USERTEST_MODE.FREE_PLAY: EndFreePlay(); break;
                    case USERTEST_MODE.RECORDING: EndScenarioRecording(); break;
                    case USERTEST_MODE.IDLE: break;
                }
            }
		}
	}

	#endregion // MONO_BEHAVIOUR



	#region METHODS_FOR_RECORDING

	void BeginScenarioRecording(int scenarioNum, BinaryModuleImpl.VIEW_MODE viewMode)
	{
        task_log[0].Clear();
        task_log[1].Clear();
        task_log[2].Clear();
        task_log[3].Clear();

		// check
		if (test_mode != USERTEST_MODE.IDLE)
		{
			Debug.Log("RECORDING: error - already launched another mode...");
			return;
		}

		if (mBinaryModuleImpl.ui_mode == BinaryModuleImpl.UI_MODE.NONE)
		{
			Debug.Log("RECORDING: error - please set the UI mode...");
			return;
		}
		mBinaryModuleImpl.view_mode = viewMode;

		// clean the container for logging
		user_log.Clear();

		// load the first task
        scenario_num = scenarioNum - 1;
		task_num = -1;
		GoNextTask();

		// set parameters
		frame_num_now = 0;
		test_mode = USERTEST_MODE.RECORDING;


        // create folder previously
        string session_str = "/session" + session_num.ToString();
        string environment_str = "/environment" + scenario_num.ToString();
        string objSize_str = "/objSize" + objSize_num.ToString();
        string method_str = "/method" + method_num.ToString();

        if (session_num == 1)
            folder_name = root_directory + "raw data/" + participant_name + "_" + m_date + "_" + m_time + session_str + environment_str + objSize_str + method_str;
        else
            folder_name = root_directory + "raw data/" + participant_name + "_" + m_date + "_" + m_time + session_str + method_str;

        System.IO.Directory.CreateDirectory(folder_name);

		Debug.Log("RECORDING: start");
	}

	void PushScenarioRecording()
	{
		//Debug.Log ("Push");
		////////////////////////////////////////
		// push user log into list
		////////////////////////////////////////
		/// 
		string record_str = frame_num_now.ToString () + " " + task_num.ToString ();
		string cam_L_str = "Oculus-L (" + ConvertTransformToString (mOculusL.transform) + ")";
		string cam_R_str = "Oculus-R (" + ConvertTransformToString (mOculusR.transform) + ")";
		string hskl_poses_str = "BONE " + mBinaryModuleImpl.hskl_error.ToString () + " (" + ConvertFloatArrayToString (mBinaryModuleImpl.GetHSKLPoseData ()) + ")";
		string tips_2D_str = "FT2D (" + ConvertFloatArrayToString (mBinaryModuleImpl.GetFT2DPossData ()) + ")";
		string tips_3D_str = "HSKL (" + ConvertFloatArrayToString (mBinaryModuleImpl.GetHSKLPossData ()) + ")";
		
		string action_str;

        if (method_num == 0)
			action_str = mBinaryModuleImpl.action_detected_grasp.ToString ();
		else if (method_num == 1)
			action_str = mBinaryModuleImpl.action_detected.ToString ();
		else if (method_num == 2)
			action_str = mBinaryModuleImpl.action_detected_SDGesture.ToString();
        else // ARWand
            action_str = mBinaryModuleImpl.action_detected_ARmarker.ToString();

		//below tip_pos_str may not be used.
		string tip_pos_str = "Tips (";
		for (int method = 0; method < 4; method++) tip_pos_str += ConvertFloatArrayToString(mBinaryModuleImpl.Get3DPointRawData(method)) + ")";
        string wand_3d_pos_str = "WAND-Pos (" + ConvertFloatArrayToString(mBinaryModuleImpl.GetARMarkerPos()) + ")";
        string wand_3d_rot_str = "WAND-Rot (" + ConvertFloatArrayToString(mBinaryModuleImpl.GetARMarkerRot()) + ")";
		string staticID = "StaticGestureID (" + mBinaryModuleImpl.currentStaticGestureID.ToString() + ")";
		//
		
		string user_str = record_str + "\n"
			+ cam_L_str + "\n" + cam_R_str + "\n"
				+ hskl_poses_str + "\n" + tips_2D_str + "\n" + tips_3D_str + "\n" 
				+ action_str + "\n" + tip_pos_str +"\n" +  wand_3d_pos_str + "\n" + wand_3d_rot_str +"\n" + staticID;
		user_log.Add(user_str);


		////////////////////////////////////////
		// task log when action detected...
		////////////////////////////////////////
		if (session_num == 1 && CheckObjectSelection() )
		{
			go_next_task = true; // lazy update
			remain_frame = GO_NEXT_TASK_FRAME;
		}
        else if(session_num == 2 && CheckObjectSelection_manipulation())
        {
            go_next_task = true; // lazy update
            remain_frame = GO_NEXT_TASK_FRAME;
        }
	}

	void EndScenarioRecording()
	{
		Debug.Log("RECORDING: end");

		// set parameters
		test_mode = USERTEST_MODE.IDLE;

		// clear the sphere
		mObjectManager.DeleteObjects();

		// write main log file
		{
			string file_name = "/main_log" +".txt";
            Debug.Log("write file: " + folder_name + file_name);
            System.IO.StreamWriter sw = System.IO.File.CreateText(folder_name + file_name);
			
			// write main log
			sw.WriteLine(session_num);
			sw.WriteLine(objSize_num);
			sw.WriteLine(scenario_num); // line #1: scenario number
			sw.WriteLine(method_num); // line #1: scenario number

			sw.WriteLine(HAND_WIDTH + " " + HAND_LENGTH); // line #2: hand width/length
			sw.WriteLine(mBinaryModuleImpl.ui_mode.ToString() + " " + mBinaryModuleImpl.view_mode.ToString() ); // line #3: user interface / view mode
			sw.WriteLine( frame_num_now ); // line #4: num of frame
			
			sw.Close();
		}
		
		// write user log file
		{
            string file_name = "/user_log.txt";
            Debug.Log("write file: " + folder_name + file_name);
            System.IO.StreamWriter sw = System.IO.File.CreateText(folder_name + file_name);
			
			// write each frame of user log...
			for (int i = 0; i < user_log.Count; i++)
			{
				string line = (string)user_log[i];
				sw.WriteLine(line);
			}
			sw.Close();
		}

        //
        if(automaticallyRecording)
            GetTaskLogWithoutReplaying();

        if (session_num == 1)
            isDone_Scneario[method_num * 12 + objSize_num * 4 + scenario_num] = true;
        else
        {
            if(method_num == 3) // arwand
                isDone_Scneario[36] = true;
            else if(method_num == 2)//sd
                isDone_Scneario[37] = true;
        }
	}

	#endregion // METHODS_FOR_RECORDING



	#region METHODS_FOR_REPLAYING

	
	void GetTaskLogWithoutReplaying()
	{
		// check first
		if (test_mode != USERTEST_MODE.IDLE)
		{
			Debug.Log("ANALYSIS: error - already launched another mode...");
			return;
		}
		BeginScenarioReplaying();
		// set parameters for replaying
		test_mode = USERTEST_MODE.ANALYSIS;
		for (frame_num_now = 0; frame_num_now <= frame_num_max; frame_num_now++)
		{
			mObjectManager.FixedUpdate();
			PopScenarioReplaying();
		}
		EndScenarioReplaying();
		//Debug.Log("333");
		test_mode = USERTEST_MODE.IDLE;
	}
	void BeginScenarioReplaying()
	{
		// check first
		if (test_mode != USERTEST_MODE.IDLE)
		{
			Debug.Log("REPLAYING: error - already launched another mode...");
			return;
		}
		
		// turn off "OVRCamera" script
		OVRCameraController controller = (OVRCameraController)Object.FindObjectOfType(typeof(OVRCameraController));
		controller.EnableOrientation = false;
		OVRCamera left_script = mOculusL.GetComponentInChildren<OVRCamera>();
		left_script.enabled = false;
		OVRCamera right_script = mOculusR.GetComponentInChildren<OVRCamera>();
		right_script.enabled = false;
		
		// clean log stack
		user_log.Clear();
		task_log[0].Clear();
		task_log[1].Clear();
		task_log[2].Clear();
		task_log[3].Clear();

		
		////////////////////////////////////////
		// read the file
		////////////////////////////////////////
		
		// read the main log
		{
			string file_name = "/main_log.txt";
            System.IO.StreamReader file;

            Debug.Log("read file: " + folder_name + file_name);
            file = new System.IO.StreamReader(folder_name + file_name);
            

			// line #1: Session number
			string session_str = file.ReadLine();
			session_num = int.Parse(session_str);

			// line #2: objSize number

			string objSize_str = file.ReadLine();
			objSize_num = int.Parse(objSize_str);

			// line #3: scenario number
			string scenario_str = file.ReadLine();
			scenario_num = int.Parse(scenario_str);


			// line #4: method number
			string method_str = file.ReadLine();
			method_num = int.Parse(method_str);
			//Debug.Log("method" + method_num);
			// line #5: hand width/length
			string hand_str = file.ReadLine();
			string[] hand_measure = hand_str.Split(delim);
			HAND_WIDTH  = float.Parse(hand_measure[0]);
			HAND_LENGTH = float.Parse(hand_measure[1]);

			mBinaryModuleImpl.ResetHSKLModel(HAND_WIDTH, HAND_LENGTH);

			// line #6: user interface / view mode
			string uiview_str = file.ReadLine();
			string[] ui_view_sub_str = uiview_str.Split(delim);

			mBinaryModuleImpl.ui_mode = (BinaryModuleImpl.UI_MODE)System.Enum.Parse(typeof(BinaryModuleImpl.UI_MODE), ui_view_sub_str[0]);
			mBinaryModuleImpl.view_mode = (BinaryModuleImpl.VIEW_MODE)System.Enum.Parse(typeof(BinaryModuleImpl.VIEW_MODE), ui_view_sub_str[1]);

			if (mBinaryModuleImpl.ui_mode == BinaryModuleImpl.UI_MODE.HAND)
			{
				//mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.ALL;
				mBinaryModuleImpl.SetHSKLModelVisibility(true, session_num);
			}
			if (mBinaryModuleImpl.ui_mode == BinaryModuleImpl.UI_MODE.MARKER)
			{
				//mBinaryModuleImpl.view_mode = BinaryModuleImpl.VIEW_MODE.INVISIBLE;
				mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
            }

			//Debug.Log(mBinaryModuleImpl.view_mode);
            if (session_num == 1)
            {
                if (objSize_num == 0)
                {
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_LARGE, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_LARGE;

                }
                else if (objSize_num == 1)
                {
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_MEDIUM, session_num);

                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_MEDIUM;

                }
                else if (objSize_num == 2)
                {
                    mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_SMALL, session_num);
                    ERROR_BOUND_POS = ObjectManagerBehaviour.SIZE_SMALL;
                }
            }

            else if (session_num == 2)
            {
                mObjectManager.SetCubeSize(ObjectManagerBehaviour.SIZE_MEDIUM, session_num);
                ERROR_BOUND_POS = 25;
                ERROR_BOUND_ROT = 30;
            }


			// line #7: num of frame
			string frame_num_str = file.ReadLine();
			frame_num_max = int.Parse(frame_num_str);
			
			file.Close();
			
			// start task
			task_num = -1;

			GoNextTask();
		}
		
		// read the user behaviour log
		{
            System.IO.StreamReader file = new System.IO.StreamReader(folder_name + "/user_log.txt");
			
			string line;
			while ((line = file.ReadLine()) != null)
			{
				user_log.Add(line);
			}
			
			file.Close();
		}
		
		// set parameters for replaying
		frame_num_now = 0;
		test_mode = USERTEST_MODE.REPLAYING;
		Debug.Log("Replaying: start");
	}

    bool firstFrame_from_prevTask = true;
    int startFrame = 0;

	void PopScenarioReplaying()
	{
		if (frame_num_now >= frame_num_max) return;
		
		////////////////////////////////////////////////////////////
		// read line:
		////////////////////////////////////////////////////////////
		int nLine = 11;
		string record_str = (string)user_log[nLine * frame_num_now + 0]; // {# of frame} {# of task}
		string oculus_L_str = (string)user_log[nLine * frame_num_now + 1]; // oculus-L ( .... ... )
		string oculus_R_str = (string)user_log[nLine * frame_num_now + 2]; // oculus-R ( .... ... )
		string hskl_pose_str = (string)user_log[nLine * frame_num_now + 3]; // BONE {error} ( {17x7} )
		string tips_2D_str = (string)user_log[nLine * frame_num_now + 4]; // FT2D ( ... ... ... ... ... )
		string tips_3D_str = (string)user_log[nLine * frame_num_now + 5]; // HSKL ( ... ... ... ... ... )
		string action_str = (string)user_log[nLine * frame_num_now + 6]; // {True} or {False} 
		string tips_pos_str = (string)user_log[nLine * frame_num_now + 7]; // Tips ( ... ... ... )
		string ARWand_pos_str = (string)user_log[nLine * frame_num_now + 8];
		string ARWand_rot_str = (string)user_log[nLine * frame_num_now + 9]; 
		string staticId_str = (string)user_log[nLine * frame_num_now + 10]; 

		string[] record_elem = record_str.Split(delim);
		
		int frame_num_record = int.Parse(record_elem[0]);
		int task_num_record = int.Parse(record_elem[1]);
		
		if ( frame_num_record!= frame_num_now)
			Debug.Log("error in frame = "+frame_num_record);



		if (task_num != task_num_record)
		{

            if (firstFrame_from_prevTask)
            {
                Debug.Log("start frame");
                firstFrame_from_prevTask = false;
                startFrame = frame_num_now;
            }
            task_num = task_num_record;
            SetTaskNum(task_num);
		}
		
		////////////////////////////////////////////////////////////
		// manipulate oculus cameras
		////////////////////////////////////////////////////////////
		{
			string cam_L = oculus_L_str.Substring(10);
			string cam_R = oculus_R_str.Substring(10);
			string[] cam_L_elem = cam_L.Split(delim);
			string[] cam_R_elem = cam_R.Split(delim);
			
			float rot_x = float.Parse(cam_L_elem[0]);
			float rot_y = float.Parse(cam_L_elem[1]);
			float rot_z = float.Parse(cam_L_elem[2]);
			float rot_w = float.Parse(cam_L_elem[3]);
			
			float pos_x = float.Parse(cam_L_elem[4]);
			float pos_y = float.Parse(cam_L_elem[5]);
			float pos_z = float.Parse(cam_L_elem[6]);
			
			Quaternion oculus_L_rot = new Quaternion(rot_x, rot_y, rot_z, rot_w);
			Vector3 oculus_L_pos = new Vector3(pos_x, pos_y, pos_z);
			
			mOculusL.transform.position = oculus_L_pos;
			mOculusL.transform.rotation = oculus_L_rot;
			
			rot_x = float.Parse(cam_R_elem[0]);
			rot_y = float.Parse(cam_R_elem[1]);
			rot_z = float.Parse(cam_R_elem[2]);
			rot_w = float.Parse(cam_R_elem[3]);
			
			pos_x = float.Parse(cam_R_elem[4]);
			pos_y = float.Parse(cam_R_elem[5]);
			pos_z = float.Parse(cam_R_elem[6]);
			
			Quaternion oculus_R_rot = new Quaternion(rot_x, rot_y, rot_z, rot_w);
			Vector3 oculus_R_pos = new Vector3(pos_x, pos_y, pos_z);
			
			mOculusR.transform.position = oculus_R_pos;
			mOculusR.transform.rotation = oculus_R_rot;
		}
		
		////////////////////////////////////////////////////////////
		// manipulate HSKL hand & fingertips
		////////////////////////////////////////////////////////////
		{
			// HSKL error & poses
			string hand = hskl_pose_str.Substring(5);
			string[] hand_elem = hand.Split(delim);
			
			float error = float.Parse(hand_elem[0]);
			float[] pose_float = new float[17 * 7];
			for (int i = 0; i < 17 * 7; i++)
			{
				pose_float[i] = float.Parse(hand_elem[i+2]);
			}
			// 2D FingerTip positions
			string ft2d = tips_2D_str.Substring(6);
			string[] ft2d_elem = ft2d.Split(delim);
			
			float[] ft2d_float = new float[5 * 3];
			for (int i = 0; i < 5 * 3; i++)
			{
				ft2d_float[i] = float.Parse(ft2d_elem[i]);
			}
			
			// 3D FingerTip positions
			string ft3d = tips_3D_str.Substring(6);
			string[] hskl_elem = ft3d.Split(delim);
			
			float[] hskl_float = new float[5 * 3];
			for (int i = 0; i < 5 * 3; i++)
			{
				hskl_float[i] = float.Parse(hskl_elem[i]);
			}
			
			// action detection result
            //string action_elem = action_str;

            bool action = false;
            SerialConnector.BUTTON_STATE action_session2 = SerialConnector.BUTTON_STATE.IDLE ;
			
            if(method_num == 0 || method_num == 1)
                action = bool.Parse(action_str);
            else 
				action_session2 = (SerialConnector.BUTTON_STATE)System.Enum.Parse(typeof(SerialConnector.BUTTON_STATE), action_str);
            
                        		
			// FingerTip
			float[] ft2d_tip_float = new float[3];
			float[] hskl_tip_float = new float[3];
			float[] tape_tip_float = new float[3];
            float[] palmCenter_float = new float[3];
			//float[] armarker_float = new float[3];

			string tip_str = tips_pos_str.Substring(6);
			string[] tip = tip_str.Split(delim);
			for (int i = 0; i < 3; i++) ft2d_tip_float[i] = float.Parse(tip[i + 0]);
			for (int i = 0; i < 3; i++) hskl_tip_float[i] = float.Parse(tip[i + 3]);
			for (int i = 0; i < 3; i++) tape_tip_float[i] = float.Parse(tip[i + 6]);
            for (int i = 0; i < 3; i++) palmCenter_float[i] = float.Parse(tip[i + 9]);



			//Palm pos

			//wand pos and rotation.
            string wand_pos = ARWand_pos_str.Substring(10);
            string[] wand_pos_elem = wand_pos.Split(delim);
            string wand_rot = ARWand_rot_str.Substring(10);
            string[] wand_rot_elem = wand_rot.Split(delim);

            float[] wand_pos_float = new float[3];
            for (int i = 0; i < 3; i++) wand_pos_float[i] = float.Parse(wand_pos_elem[i]);
            float[] wand_rot_float = new float[4];
            for (int i = 0; i < 4; i++) wand_rot_float[i] = float.Parse(wand_rot_elem[i]);

			//static id
			string [] staticId = staticId_str.Substring(17).Split(delim);
//			Debug.Log(staticId[0]);
			int staticID = int.Parse(staticId[0]);
			

			mBinaryModuleImpl.UpdateByData(error, pose_float, ft2d_float, hskl_float,
			                               action, ft2d_tip_float, hskl_tip_float, tape_tip_float,palmCenter_float,
			                               wand_pos_float, wand_rot_float, action_session2,staticID);
		}
		
		////////////////////////////////////////////////////////////
		// sync with ObjectManager
		////////////////////////////////////////////////////////////
        if (session_num == 1 )
        {
            CheckObjectSelection();
        }
        else if (session_num == 2)
        {
            CheckObjectSelection_manipulation();
        }
	}
	
	void EndScenarioReplaying()
	{
		// set parameters
		test_mode = USERTEST_MODE.IDLE;
		
		// clear the sphere
		mObjectManager.DeleteObjects();
		
		// turn on "OVRCamera" script
		OVRCameraController controller = (OVRCameraController)Object.FindObjectOfType(typeof(OVRCameraController));
		controller.EnableOrientation = true;
		OVRCamera left_script = mOculusL.GetComponentInChildren<OVRCamera>();
		left_script.enabled = true;
		OVRCamera right_script = mOculusR.GetComponentInChildren<OVRCamera>();
		right_script.enabled = true;
		
		////////////////////////////////////////////////////////////
		// write diff file **************************************************************************************************************************************************************** TODO
		////////////////////////////////////////////////////////////

		//string file_name;
		////////////////////////////////////////////////////////////
		// write diff file **************************************************************************************************************************************************************** TODO
		////////////////////////////////////////////////////////////
		{
            //System.IO.Directory.CreateDirectory(folder_name);
            
            Debug.Log("write file: " + folder_name + "/analysis.txt");
            System.IO.StreamWriter sw = System.IO.File.CreateText(folder_name + "/analysis.txt");
			
			// write each frame of user log...
			//Debug.Log(method_num.ToString());
			for (int i = 0; i < task_log[method_num].Count; i++)
			{
				string line = (string)task_log[method_num][i];
				sw.WriteLine(line);
			}
			sw.Close();
		}
        mBinaryModuleImpl.SetHSKLModelVisibility(false, session_num);
		Debug.Log("Replaying: end");

        

	}
	#endregion // METHODS_FOR_REPLAYING


	#region METHODS_FOR_FREEPLAY

	private void BeginTraining()
	{
		// check first
		if (test_mode == USERTEST_MODE.RECORDING || test_mode == USERTEST_MODE.REPLAYING)
		{
			Debug.Log("FREE PLAY: error - already launched another mode...");
			return;
		}

		// set scenario
		mObjectManager.Freeplay(1, 1);

		test_mode = USERTEST_MODE.FREE_PLAY;
		Debug.Log("Training: start");
	}

    private void BeginFreePlay(bool is_training)
	{
		// check first
		if (test_mode == USERTEST_MODE.RECORDING || test_mode == USERTEST_MODE.REPLAYING)
		{
			Debug.Log("FREE PLAY: error - already launched another mode...");
			return;
		}

		// set scenario
		if (is_training) mObjectManager.Training(8, 8);
		else mObjectManager.Freeplay(8, 8);

		test_mode = USERTEST_MODE.FREE_PLAY;
		Debug.Log("FREE_PLAY: start");
	}

	private void EndFreePlay()
	{
		mObjectManager.DeleteObjects();
		test_mode = USERTEST_MODE.IDLE;
		Debug.Log("FREE_PLAY: end");
	}

	#endregion // METHODS_FOR_FREEPLAY

    
    #region SUBROUTINES

    private bool GoNextTask()
    {
        task_num++;
        if(( (session_num == 1) && (task_num < Seed[scenario_num].Length))
            || (session_num == 2) && (task_num < Seed[5].Length))
        {
            SetTaskNum(task_num);
            return true;
        }
        return false;
    }

	private void SetTaskNum(int num)
	{
		//Debug.Log("task number = " + num); // **************************************** for debug

		int horizontal = area_id[num] % 3 - 1;
		int vertical = area_id[num] / 3 - 1;


        if (session_num == 1)
        {
            switch (scenario_num)
            {
				case 0: mObjectManager.Scenario1(horizontal, vertical, Seed[0][num], 60, false, 0.0f, method_num); break;
                case 1: mObjectManager.Scenario1(horizontal, vertical, Seed[1][num], 60, true, 25.0f, method_num); break;
                case 2: mObjectManager.Scenario2(horizontal, vertical, Seed[2][num], 8, 8, false, method_num); break;
                case 3: mObjectManager.Scenario2(horizontal, vertical, Seed[3][num], 8, 8, true, method_num); break;
                case 4: mObjectManager.Scenario3(Seed[4][num], 8, 8); break;
            }
        }

        else if (session_num == 2)
        {
			mObjectManager.Scenario_Session2(horizontal, vertical, Seed[5][num], 14, false, 0.0f); 
		}

	}

	private bool prev_frame_action = false;
    //private bool prev_frame_IsMatching = false;

    private bool CheckObjectSelection()
	{
		bool selected = false;
		if (mBinaryModuleImpl.ui_mode == BinaryModuleImpl.UI_MODE.HAND)
		{
			//Debug.Log( mBinaryModuleImpl.action_detected_grasp);

			if ((mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.CAPE &&
			    mBinaryModuleImpl.action_detected && !prev_frame_action) ||
				(mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.GRAB &&
				 mBinaryModuleImpl.action_detected_grasp && !prev_frame_action) ||
			 	(mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.SD && 
			 	mBinaryModuleImpl.action_detected_SDGesture == SerialConnector.BUTTON_STATE.DOWN)
				)
			{
                //Debug.Log(mBinaryModuleImpl.action_detected_grasp);
                switch (mBinaryModuleImpl.view_mode)
				{
                    case BinaryModuleImpl.VIEW_MODE.GRAB: selected = ScenarioProcedure(0); break;
                    case BinaryModuleImpl.VIEW_MODE.CAPE: selected = ScenarioProcedure(1); break;
                    case BinaryModuleImpl.VIEW_MODE.SD: selected = ScenarioProcedure(2); break;
				}

				prev_frame_action = true;

			}
			if ((mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.CAPE &&
			     !mBinaryModuleImpl.action_detected) ||
			    (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.GRAB && 
			 !mBinaryModuleImpl.action_detected_grasp) )
			{
			    prev_frame_action = false;
			}

		}
		else //marker
		{
            //Debug.Log(mBinaryModuleImpl.action_detected_ARmarker);
            if (mBinaryModuleImpl.action_detected_ARmarker == SerialConnector.BUTTON_STATE.DOWN )
			{
                    selected = ScenarioProcedure(3);
			}
		}

		return selected;
	}

    private bool CheckObjectSelection_mouseClick()
	{
		bool selected = false;

        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit = new RaycastHit();

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                Vector3 tip = hit.point;
                int id; float error; Vector3 target;
                mObjectManager.GetClosestObjectToPoint(tip, out id, out error, out target);
                
                if (error < ERROR_BOUND_POS)
                {
                    mObjectManager.ChangeObjectColorWithID(id);
                    if (id == 0)
                    {
                        Debug.Log(hit.transform.gameObject.name);
                        selected = true;
                        Matrix4x4 mat_R = mOculusR.transform.worldToLocalMatrix;
                        // object id #0 - position in global & camera coordinates
			            GameObject obj_id0 = mObjectManager.GetObjectByID(0);
			            Vector3 obj_pos_global = obj_id0.transform.position;
			            Vector3 obj_pos_camera = mat_R.MultiplyPoint(obj_pos_global);

			            // fingertip - position in global & camera coordinates
			            // Vector3 tip_pos_global = mBinaryModuleImpl.GetTipPosData(method);
                        Vector3 tip_pos_camera = mat_R.MultiplyPoint(tip);

                        Vector3 diff = tip_pos_camera - obj_pos_camera;

				        task_log[0].Add(diff.x + "\t" + diff.y + "\t" + diff.z);
                        //count_success[method]++; // **************************************************************************************************************************************************************** TODO
                    }
                }
                			

			 
			}
         }
        return selected;
    }

    bool bSourceObjectisSelected = false;
    int id_selectedObject;
    Vector3 initialOrientation;

    private bool CheckObjectSelection_manipulation()
    {
		if (Input.GetKeyDown (KeyCode.Space))
			return true;
			
		GameObject obj_id0 = mObjectManager.GetObjectByID(0);
		GameObject obj_id1 = mObjectManager.GetObjectByID(1);

		bool selected = false;
        int id; float error; Vector3 target;

		SerialConnector.BUTTON_STATE buttonState;

		if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.SD)
			buttonState = mBinaryModuleImpl.action_detected_SDGesture;
		else
			buttonState = mBinaryModuleImpl.action_detected_ARmarker;

		GameObject cursor;

		if (mBinaryModuleImpl.view_mode == BinaryModuleImpl.VIEW_MODE.MARKER_UI)
			cursor = mBinaryModuleImpl.cursor_arwand;
		else
			cursor = mBinaryModuleImpl.cursor_sd;

        if (buttonState == SerialConnector.BUTTON_STATE.DOWN)
        {
            //marker's pos
			Vector3 tip = mBinaryModuleImpl.GetTipPosData(method_num); 

            mObjectManager.GetClosestObjectToPoint(tip, out id, out error, out target);

            //is there selected object.
            if (error < ERROR_BOUND_POS && id == 0)
            {
				float Rx_x = Vector3.Angle(cursor.transform.right, obj_id0.transform.right);
				float Ry_y = Vector3.Angle(cursor.transform.up, obj_id0.transform.up);
				float Ry_z = Vector3.Angle(cursor.transform.up, obj_id0.transform.forward);

				Debug.Log(Rx_x.ToString() + " " + Ry_y.ToString() + " " + Ry_z.ToString());
				if( ((Rx_x < ERROR_BOUND_ROT && Ry_y < ERROR_BOUND_ROT) || (Rx_x < ERROR_BOUND_ROT && Ry_z < ERROR_BOUND_ROT))
				   ||
				   ((Rx_x > 180-ERROR_BOUND_ROT && Ry_y > 180-ERROR_BOUND_ROT) || (Rx_x > 180-ERROR_BOUND_ROT && Ry_z > 180- ERROR_BOUND_ROT))
				   ||
				   ((Rx_x > 180-ERROR_BOUND_ROT && Ry_z < ERROR_BOUND_ROT) || (Rx_x < ERROR_BOUND_ROT && Ry_z > 180-ERROR_BOUND_ROT))

				   )
				{
                    
	                mObjectManager.ChangeObjectSpecificColorWithID(0, new Color(0.0f, 0.5f, 1.0f, 0.7f));
	                bSourceObjectisSelected = true;
					initialOrientation = cursor.transform.rotation.eulerAngles;

                    /*
                     * save source-marker diff. it is not used.
                    //get diff of position.
                    Matrix4x4 mat_R = mOculusR.transform.worldToLocalMatrix;
                    Vector3 marker_pos_camera = mat_R.MultiplyPoint(tip);
                    Vector3 obj0_pos_camera = mat_R.MultiplyPoint(obj_id0.transform.position);
                    Vector3 diff = marker_pos_camera - obj0_pos_camera;
                    
                    //
                    float maximum = (Ry_y > Ry_z) ? Ry_y : Ry_z;
                    task_log[3].Add(diff.x + "\t" + diff.y + "\t" + diff.z + "\t" + Rx_x + "\t");
                    */
				}
                
            }
            else
            {
                //bSourceObjectisSelected = false;
            }

        }
        else if (buttonState == SerialConnector.BUTTON_STATE.UP) 
        {
            if (bSourceObjectisSelected)
            {
				mObjectManager.ChangeObjectSpecificColorWithID(0, new Color(0.0f, 1.0f, 1.0f, 0.7f));
                //mObjectManager.ChangeObjectSpecificColorWithID(1, Color.gray);
                bSourceObjectisSelected = false;
            }
        }

        if(bSourceObjectisSelected)
        {
			Vector3 diffVec = cursor.transform.rotation.eulerAngles - initialOrientation;
            Debug.Log(diffVec);
			obj_id0.transform.position = cursor.transform.position;
			obj_id0.transform.rotation = cursor.transform.rotation;

			//obj_id0.transform.Rotate(diffVec, Space.World);
			initialOrientation = cursor.transform.rotation.eulerAngles;
//          Debug.Log(initialOrientation);
        }


		//check distance & orientation.
				
		Matrix4x4 mat_R = mOculusR.transform.worldToLocalMatrix;

        Vector3 obj0_pos = obj_id0.transform.position;
        Vector3 obj1_pos = obj_id1.transform.position;

		
		// it needs be modified. and mouse up
		if(Vector3.Distance(obj0_pos, obj1_pos) < ERROR_BOUND_POS )
        {
			float Rx_x = Vector3.Angle(-obj_id0.transform.right, obj_id1.transform.right);
			float Ry_y = Vector3.Angle(-obj_id0.transform.up, obj_id1.transform.up);
			float Ry_z = Vector3.Angle(-obj_id0.transform.up, obj_id1.transform.forward);

			Debug.Log(Rx_x.ToString() + " " + Ry_y.ToString() + " " + Ry_z.ToString());
			if( ((Rx_x < ERROR_BOUND_ROT && Ry_y < ERROR_BOUND_ROT) || (Rx_x < ERROR_BOUND_ROT && Ry_z < ERROR_BOUND_ROT))
			   ||
			   ((Rx_x > 180-ERROR_BOUND_ROT && Ry_y > 180-ERROR_BOUND_ROT) || (Rx_x > 180-ERROR_BOUND_ROT && Ry_z > 180- ERROR_BOUND_ROT))
			   ||
			   ((Rx_x > 180-ERROR_BOUND_ROT && Ry_z < ERROR_BOUND_ROT) || (Rx_x < ERROR_BOUND_ROT && Ry_z > 180-ERROR_BOUND_ROT))
			   )
			{
				Vector3 obj0_pos_camera = mat_R.MultiplyPoint(obj0_pos);
				Vector3 obj1_pos_camera = mat_R.MultiplyPoint(obj1_pos);
				Vector3 diff = obj1_pos_camera - obj0_pos_camera;
				
				Color selectColor = new Color(1.0f, 0.0f, 0.6f, 0.7f);
				//mObjectManager.ChangeObjectSpecificColorWithID(1, new Color(1.0f, 0.0f, 0.5f, 0.7f));

				
				if (buttonState == SerialConnector.BUTTON_STATE.UP)
				{
					bSourceObjectisSelected = false;
					mObjectManager.ChangeObjectColorWithID(1, selectColor);
					selected = true;

                    int spent_frame = frame_num_now - startFrame;
                    firstFrame_from_prevTask = true;

                    float minimum = (Ry_y < Ry_z) ? Ry_y : Ry_z;
                    task_log[3].Add(diff.x + "\t" + diff.y + "\t" + diff.z + "\t" + Rx_x + "\t" + minimum + "\t" + (spent_frame*Time.fixedDeltaTime).ToString());

                    //Debug.Log(task_log[3][task_log[3].Count - 1]);
				}

			}
			mObjectManager.ChangeObjectSpecificColorWithID(1, new Color(1.0f, 0.0f, 1.0f, 0.7f));

			//prev_frame_IsMatching = false;
            
        }
      

        return selected;
    }
	
	private bool ScenarioProcedure(int method)
	{
		bool selected = false;
		int id; float error_toClosestObject; float error_toTargetObject; Vector3 target;

		// get the closest object #id & distance

        Vector3 tip = mBinaryModuleImpl.GetTipPosData(method);
        mObjectManager.GetClosestObjectToPoint(tip, out id, out error_toClosestObject, out target);
        mObjectManager.GetTargetObjectToPoint(tip, out error_toTargetObject);
		Debug.Log ("tip : " + tip.ToString () + " " + "target : " + target.ToString ());
		// if error is in the bound, set the object #id as a selected.
		if (error_toClosestObject < ERROR_BOUND_POS)
		{
			mObjectManager.ChangeObjectColorWithID(id);
        }

        if (error_toTargetObject < ERROR_BOUND_POS)
        {
            selected = true;
            //save succece count;
        }
        
        else if (error_toTargetObject < ObjectManagerBehaviour.SIZE_LARGE)
        {
            selected = true;
            //skip this stage but, not add the success count.
        }


		////////////////////////////////////////
		// stack the data into log... **************************************************************************************************************************************************************** TODO
		////////////////////////////////////////
		//if (test_mode == USERTEST_MODE.REPLAYING || test_mode == USERTEST_MODE.ANALYSIS) 
		{
			Matrix4x4 mat_R = mOculusR.transform.worldToLocalMatrix;

			// object id #0 - position in global & camera coordinates
			GameObject obj_id0 = mObjectManager.GetObjectByID(0);
			Vector3 obj_pos_global = obj_id0.transform.position;
			Vector3 obj_pos_camera = mat_R.MultiplyPoint(obj_pos_global);

			// fingertip - position in global & camera coordinates
			Vector3 tip_pos_global = mBinaryModuleImpl.GetTipPosData(method);
			Vector3 tip_pos_camera = mat_R.MultiplyPoint(tip_pos_global);

			//float[] raw = mBinaryModuleImpl.Get3DPointRawData(method);
			//Vector3 pos = new Vector3(raw[0], raw[1], raw[2]);

			Vector3 diff = tip_pos_camera - obj_pos_camera;

			task_log[method].Add(diff.x + "\t" + diff.y + "\t" + diff.z);
			
        }

		return selected;
	}

	#endregion // SUBROUTINES
    


	#region CONVERT_TO_STRING

	private string ConvertTransformToString(Transform transform)
	{
		Vector3    pos = transform.position;
		Quaternion rot = transform.rotation;

		string transform_str = "";
		transform_str += rot.x.ToString() + "," + rot.y.ToString() + "," + rot.z.ToString() + "," + rot.w.ToString() + ",";
		transform_str += pos.x.ToString() + "," + pos.y.ToString() + "," + pos.z.ToString();

		return transform_str;
	}

	private string ConvertFloatArrayToString(float[] array)
	{
		string array_str = "";
		for (int i = 0; i < array.Length-1; i++)
			array_str += array[i].ToString() + ",";
		array_str += array[array.Length - 1].ToString();
		return array_str;
	}

    private string ConvertVector3ToString(Vector3 vec)
	{
		string vec_str = vec.x.ToString() + "," + vec.y.ToString() + "," + vec.z.ToString();
		return vec_str;
	}
    
	#endregion // CONVERT_TO_STRING
}
