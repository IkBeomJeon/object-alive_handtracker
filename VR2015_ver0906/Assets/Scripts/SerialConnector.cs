﻿using System.Collections;
using System.IO.Ports;
using System.Threading;

public class SerialConnector
{
    SerialPort mySerial;

    Thread t1;
    bool isOn = true;
    public enum BUTTON_STATE { DOWN, UP, STAY, IDLE }
   
    public BUTTON_STATE ButtonActionState { get; set; }

    public ArrayList buttonValueList = new ArrayList();

    // Use this for initialization
    public SerialConnector(string ComPort1)
    {
        mySerial = new SerialPort(ComPort1, 9600, Parity.None, 8, StopBits.One);
        StartSerialComm();

        t1 = new Thread(new ThreadStart(UpdateSerial));

        t1.Start();
        ButtonActionState = BUTTON_STATE.IDLE;
    }

    // Update is called once per frame


    void UpdateSerial()
    {
        while (isOn)
        {
            //serial 1
            if (mySerial != null && mySerial.IsOpen)
            {
                string receiveData = mySerial.ReadLine();
                double currButtonPushValue = (receiveData == "1") ? 0.0 : 1.0;

                buttonValueList.Add(currButtonPushValue);
                if (buttonValueList.Count > 20)
                {
                    buttonValueList.RemoveAt(0);
                    ButtonActionState = getAction();
                }
              

            }
        }
    }

    public BUTTON_STATE getAction()
    {
        double average = AverageButtonValueList();

        if (average >= 0.05 && (double)buttonValueList[0] == 0.0)
            return BUTTON_STATE.DOWN; //down
        else if (average > 0.05 && average < 1.0 && (double)buttonValueList[0] == 1.0)
             return BUTTON_STATE.UP;
        else if(average == 1.0)
            return BUTTON_STATE.STAY;
        else 
            return BUTTON_STATE.IDLE;
    }
    double AverageButtonValueList()
    {
        double sum = 0;
        for(int i=0; i<buttonValueList.Count; i++)
        {
            sum += (double)buttonValueList[i];
        }
        return sum / buttonValueList.Count;
    }
    void StartSerialComm()
    {
        
        if (mySerial != null && mySerial.IsOpen)
        {
            //Debug.Log("Serial is aleady open!");
            mySerial.Close();
        }
        mySerial.Open();
        if (mySerial.IsOpen)
        {
            //Debug.Log("Serial is Connected!");
        }
        

    }
    public void OnDestroy()
    {
        isOn = false;
		Thread.Sleep (10);
        if (mySerial.IsOpen)
            mySerial.Close();

    }

}
