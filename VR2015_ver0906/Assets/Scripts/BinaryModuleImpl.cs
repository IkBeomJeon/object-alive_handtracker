using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
public class BinaryModuleImpl
{
	#region CONSTANTS

	public enum VIEW_MODE { INVISIBLE, FT2D, HSKL, CAPE, ALL, GRAB, SD, MARKER_UI}
	public enum UI_MODE { NONE, HAND, MARKER }
	const int RESET_FRAME = 30; // 30/60 [sec] = 0.5 second

	const string NATIVE_DLL_NAME = "VR2015";
	const string NATIVE_DLL_NAME_SD = "Object Alive_Online";

	const float HAND_SIZE_SCALE = 1000.0f;
	const float TRANSLATE_SCALE = 1000.0f;

	const float torelance = 20.0f;

	Vector3 FingertipSize = new Vector3(20, 20, 20);

	// depth image resolution
	const int DEPTH_WIDTH = 320;
	const int DEPTH_HEIGHT = 240;

	// sub-resolution for raw-depth mesh
	const int WIDTH = DEPTH_WIDTH / 2;
	const int HEIGHT = DEPTH_HEIGHT / 2;

	const float z_offset_pointcloud = 10.0f;

	Color skinColor;
    Color skinColor_transparent;
	Color failColor;

    string COMPORT = "COM5";
	#endregion // CONSTANTS



	#region NATIVE_FUNTIONS

	// class RGBDCamera
	[DllImport(NATIVE_DLL_NAME)]
	public static extern void initBinaryModule(float hand_width, float hand_length);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void updateBinaryModule(bool handTracking);
	[DllImport(NATIVE_DLL_NAME)]
    public static extern void endBinaryModule();

	// getter for raw-depth (point cloud / mesh)
	[DllImport(NATIVE_DLL_NAME)]
	private static extern IntPtr getPointCloud(out int size);

	// getters for HSKL
	[DllImport(NATIVE_DLL_NAME)]
	private static extern float getHandError();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getHandMeshVertices(float[] hand_verts);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getBoneModelMatrices(float[] hskl_poses);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void resetHandSize(float hand_w, float hand_h);

	// getters for fingertips
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFingerTipsFromHSKL(float[] pos_list);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFingerTipsFrom2D(float[] pos_list);
    [DllImport(NATIVE_DLL_NAME)]
    private static extern void getPalmPos_DT(float[] pos);

    // getters for CAPE
	[DllImport(NATIVE_DLL_NAME)]
	private static extern bool getAction();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern bool getAction_grasp();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getFT2DTip(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getHSKLTip(float[] pos);
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getTAPETip(float[] pos);

     
    // ARToolkit modules
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int initMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern int runMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	public static extern void endMarkerTracking();
	[DllImport(NATIVE_DLL_NAME)]
	private static extern void getMarkerPos(float[] pos);
    [DllImport(NATIVE_DLL_NAME)]
    private static extern void getMarkerRotation(float[] pos);
    [DllImport(NATIVE_DLL_NAME)]
    private static extern int[] GetMarkerNum_test();

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // For SD Gesture
    ////////////////////////////////////////////////////////////////////////////////////////////////////

	[DllImport(NATIVE_DLL_NAME_SD)]
	public static extern void getSDGesture(out int staticID, out int grabStatus);
	
	// class RGBDCamera
    [DllImport(NATIVE_DLL_NAME_SD)]
    public static extern void initBinaryModule_SD(float hand_width, float hand_length);
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern void updateBinaryModule_SD(bool handTracking);
    [DllImport(NATIVE_DLL_NAME_SD)]
    public static extern void endBinaryModule_SD();

    // getter for raw-depth (point cloud / mesh)
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern IntPtr getPointCloud_SD(out int size);

    // getters for HSKL
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern float getHandError_SD();
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern void getHandMeshVertices_SD(float[] hand_verts);
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern void getBoneModelMatrices_SD(float[] hskl_poses);

    // getters for fingertips

    // getters for fingertips
    [DllImport(NATIVE_DLL_NAME_SD)]
    private static extern void getFingerTipsFromHSKL_SD(float[] pos_list);

	[DllImport(NATIVE_DLL_NAME_SD)]
	private static extern void resetHandSize_SD(float hand_w, float hand_h);

	#endregion // NATIVE_FUNTIONS
	
	
	
	#region PUBLIC_MEMBERS

	public GameObject mHSKLModel = null;
	public GameObject mFiveFingers = null;
	public GameObject mActionRecog = null;
    public GameObject mPalmCenter_2D = null;
	public GameObject mMeshVisualizer = null;

	// HSKL sub-mesh data
	public GameObject[] bones;
	public Mesh[] meshes;

	// fingertip data
	public GameObject[] tips_FT2D;
	public GameObject[] tips_HSKL;
    public GameObject palmCenter_2D;

	// CAPE
	public GameObject tip_FT2D_dummy;
	public GameObject tip_HSKL_dummy;
	public GameObject tip_TAPE_dummy;
	public GameObject tip_FT2D;
	public GameObject tip_HSKL;
	public GameObject tip_TAPE;
    public GameObject tip_SD; //not implemented yet.

	// Cursor
	public GameObject cursor_arwand;
	public GameObject cursor_sd;

	// raw mesh visualizer
	public Mesh meshForHand;
    public SerialConnector mSerialConnector;
	#endregion // PUBLIC_MEMBERS


	#region PRIVATE_MEMBERS

	Vector3 palm_pos = Vector3.zero;
    Vector3 palmCenter_DT_prev;
	float[] hskl_poses = new float[17 * 7];
	float[] ft2d_poss = new float[5 * 3];

	float[] hskl_poss = new float[5 * 3];
	float[] tip_FT2D_pos = new float[3];
	float[] tip_HSKL_pos = new float[3];
	float[] tip_TAPE_pos = new float[3];
    float[] palm_2D_pos = new float[3];

    float[] armarker_pos = new float[3];
	float[] armarker_rotation = new float[4];

	float[] floatArray = new float[WIDTH * HEIGHT * 3];


	int prevGrabStatus = 0;

	private Queue<bool> visibleFrame = new Queue<bool>();

	#endregion // PRIVATE_MEMBERS

	#region PUBLIC_METHODS_FOR_USERTEST

	public UI_MODE ui_mode { get; set; }
	public VIEW_MODE view_mode { get; set; }

	public float hskl_error { get; set; }
    public bool action_detected{ get; set; }
	public bool action_detected_grasp { get; set; }
    public SerialConnector.BUTTON_STATE action_detected_ARmarker { get; set; }
	public int currentStaticGestureID = -1;
    public SerialConnector.BUTTON_STATE action_detected_SDGesture { get; set; }
	public int num_of_marker { get; set; }
	public float[] GetHSKLPoseData() { return hskl_poses; }
	public float[] GetFT2DPossData() { return ft2d_poss; }
	public float[] GetHSKLPossData() { return hskl_poss; }

	public void ResetHSKLModel(float hand_width, float hand_length)
	{
		if(view_mode != VIEW_MODE.SD)
			resetHandSize(hand_width, hand_length);
		else
			resetHandSize_SD(hand_width, hand_length);

		for (int i = 0; i < 17; i++)
		{
			UnityEngine.Object.Destroy((UnityEngine.Object)bones[i]);
			UnityEngine.Object.Destroy((UnityEngine.Object)meshes[i]);

		}

		InitHSKLHandMesh(mHSKLModel);
		InitSDCursor( bones[1]); // SD cursor
	}


    public float[] GetARMarkerPos()
    {
        return armarker_pos;
    }

    public float[] GetARMarkerRot()
    {
        return armarker_rotation;
    }
	public float[] Get3DPointRawData(int method_id_lagercy)
	{
		float[] tips = new float[3];

        // iJeon commented.
		// **warning : method_id not mean method_num in UsertestManagerBehaviour.cs
        // iJeon 
        if (method_id_lagercy == 0) return tip_FT2D_pos;
        if (method_id_lagercy == 1) return tip_HSKL_pos;
        if (method_id_lagercy == 2) return tip_TAPE_pos;
        if (method_id_lagercy == 3) return palm_2D_pos;

		return tips;
	}

	public Vector3 GetTipPosData(int num)
	{
		// tape data
        if (num == 0)
        {
            //float y = (bones[1].transform.position.y + bones[8].transform.position.y) / 2.0f;
            //return new Vector3(bones[1].transform.position.x, y, bones[1].transform.position.z);
            return palmCenter_2D.transform.position;

        }
		if (num == 1) return tip_TAPE.transform.position;
		if (num == 2) return cursor_sd.transform.position;
		if (num == 3) return cursor_arwand.transform.position;

		return Vector3.zero;
	}
 
	public void SetHSKLModelVisibility(bool visible, int session_num)
	{
		if (ui_mode == UI_MODE.HAND)
		{
			// FT2D fingertips
			for (int i = 0; i < 5; i++) tips_FT2D[i].renderer.enabled = true;

			// HSKL bones
            if (visible)
            {
                for (int id = 1; id < 17; id++)
                {
                    bones[id].renderer.enabled = true;
                }
            }
            else
            {
                for (int id = 1; id < 17; id++)
                {
                    if (id == 14 || id == 1)
                        bones[id].renderer.enabled = true;
                    else
                        bones[id].renderer.enabled = false;
                }
            }
            
            //if (view_mode == VIEW_MODE.GRAB)
            //    palmCenter_2D.SetActive(true);
            //else
            palmCenter_2D.SetActive(false);

            //cursor_arwand.renderer.enabled = false;
          
            mMeshVisualizer.SetActive(true);

            if (view_mode == VIEW_MODE.GRAB)
            {
                cursor_arwand.SetActive(false);
				cursor_sd.SetActive(false);
                bones[1].renderer.material.color = skinColor_transparent;
                bones[14].renderer.material.color = skinColor_transparent;

                for (int id = 0; id < 5; id++)
                {
                    tips_FT2D[id].renderer.enabled = false;
                }
            }
            else if(view_mode == VIEW_MODE.CAPE)
            {
                cursor_arwand.SetActive(false);
				cursor_sd.SetActive(false);

                bones[1].renderer.material.color = skinColor;
                bones[14].renderer.material.color = skinColor;
                
                for (int id = 0; id < 5; id++)
                {
                    tips_FT2D[id].renderer.enabled = true;
                }
            }
            else if (view_mode == VIEW_MODE.SD)
            {
				cursor_sd.SetActive(true);
                cursor_arwand.SetActive(false);

                if (session_num == 1)
                {
                    cursor_sd.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
                    Debug.Log("active false sdcursor");
                }
                else
                {
                    cursor_sd.transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
                    Debug.Log("active true sdcursor");
                }


                bones[1].renderer.material.color = skinColor;
                bones[14].renderer.material.color = skinColor;
            }
		}
		else
		{
            //cursor_arwand.transform.parent = mFiveFingers.transform;
            //cursor_arwand.renderer.enabled = true;
            cursor_arwand.SetActive(true);

			cursor_sd.SetActive(false);

            palmCenter_2D.SetActive(false);
            mMeshVisualizer.SetActive(false);

			// FT2D fingertips
			for (int i = 0; i < 5; i++) tips_FT2D[i].renderer.enabled = false;

			// HSKL bones
			for (int id = 1; id < 17; id++)
			{
				bones[id].renderer.enabled = false;
			}
		}
	}

	public void FixedUpdate()
	{
		// reset the visibility of fingertip
		if (visibleFrame.Count > 0)
		{
			visibleFrame.Dequeue();

			// change the index finger color into yellow.
            tips_FT2D[3].renderer.material.color = new Color(1.0f, 1.0f, 0f, 0.5f);//Color.yellow;

			if (view_mode == VIEW_MODE.FT2D || view_mode == VIEW_MODE.ALL) tip_FT2D.renderer.enabled = true;
			if (view_mode == VIEW_MODE.HSKL || view_mode == VIEW_MODE.ALL) tip_HSKL.renderer.enabled = true;
			if (view_mode == VIEW_MODE.CAPE || view_mode == VIEW_MODE.ALL) tip_TAPE.renderer.enabled = true;

		}
		else
		{
			// return to magenta
			tips_FT2D[3].renderer.material.color = skinColor_transparent;

			tip_FT2D.renderer.enabled = false;
			tip_HSKL.renderer.enabled = false;
			tip_TAPE.renderer.enabled = false;
		}
	}
    

	#endregion // PUBLIC_METHODS_FOR_USERTEST



	#region MAIN_ROUTINE

	public BinaryModuleImpl(float hand_width, float hand_length)
    {
        // Initialize binary module
        initBinaryModule(hand_width, hand_length);

        // skin color
        skinColor = new Color(1.0f, 0.8f, 0.6f);
        skinColor_transparent = new Color(1.0f, 0.8f, 0.6f, 0.5f);
        failColor = new Color(0.6f, 0.4f, 0.2f);

        // Initialize hand (HSKL, fingertips, CAPE) modules
        mHSKLModel = new GameObject("HSKL_HandModel");
        mHSKLModel.transform.parent = GameObject.Find("CameraRight").transform;
        mHSKLModel.transform.localPosition = new Vector3(-0.032f, 0, 0);
        mHSKLModel.transform.localRotation = new Quaternion();
        InitHSKLHandMesh(mHSKLModel);

        // for grab and cape
        {
            // 5 Fingertips (FT2D, HSKL)
            mFiveFingers = new GameObject("Fingertips");
            mFiveFingers.transform.parent = GameObject.Find("CameraRight").transform;
            mFiveFingers.transform.localPosition = new Vector3(-0.032f, 0, 0);
            mFiveFingers.transform.localRotation = new Quaternion();
            InitFingertips(mFiveFingers);

            //1 Palm Center Extracted Distance transform
            mPalmCenter_2D = new GameObject("PalmCenter_DT");
            mPalmCenter_2D.transform.parent = GameObject.Find("CameraRight").transform;
            mPalmCenter_2D.transform.localPosition = new Vector3(-0.032f, 0, 0);
            mPalmCenter_2D.transform.localRotation = new Quaternion();
            InitPalmCenter_DT(mPalmCenter_2D);

            // index fingertip for CAPE
            mActionRecog = new GameObject("CAPE");
            //		mActionRecog.transform.parent = GameObject.Find("CameraRight").transform;
            //		mActionRecog.transform.localPosition = new Vector3(-0.032f, 0, 0);
            mActionRecog.transform.parent = GameObject.Find("VirtualEnvironment").transform;
            mActionRecog.transform.localPosition = Vector3.zero;
            mActionRecog.transform.localRotation = Quaternion.identity;
            InitTAPE(mFiveFingers, mActionRecog);

			// init cursor
            InitARWandCuror(mFiveFingers); // AR Marker
			InitSDCursor( bones[1]); // SD cursor


            // set initial parameters
            hskl_error = 1.0f;
            action_detected = false;
            action_detected_grasp = false; //grasp
            tip_FT2D.renderer.enabled = false;
            tip_HSKL.renderer.enabled = false;
            tip_TAPE.renderer.enabled = false;
        }

        ui_mode = UI_MODE.HAND;
        view_mode = VIEW_MODE.GRAB;


        // Point Cloud in the Oculus Camera hierarchy (for using oculus rotation)
        mMeshVisualizer = GameObject.Find("MeshVisualizer");
        mMeshVisualizer.transform.parent = GameObject.Find("CameraRight").transform;
        mMeshVisualizer.transform.localPosition = new Vector3(-0.032f, 0, z_offset_pointcloud);
        mMeshVisualizer.transform.localRotation = new Quaternion();
        InitMeshVisualizer(mMeshVisualizer);

        //Initialize Serial Connector.
		SetHSKLModelVisibility(true, 1);

        //mSerialConnector = new SerialConnector(COMPORT);

    }


    bool prevDownState = false;
    bool prevUpState = false;


	public void UpdateByLiveFrame()
	{
		// update live camera
	

		// copy data from binary module
		if (ui_mode == UI_MODE.HAND)
		{

            if (view_mode == VIEW_MODE.GRAB)
            {
                updateBinaryModule(true);
                hskl_error = getHandError();
                getBoneModelMatrices(hskl_poses);

                getFingerTipsFrom2D(ft2d_poss);
                getFingerTipsFromHSKL(hskl_poss);

                getFT2DTip(tip_FT2D_pos);
                getHSKLTip(tip_HSKL_pos);
                getTAPETip(tip_TAPE_pos);

                bool aligned_palmCenter = ManipulatePalmCenter2D();

                getPalmPos_DT(palm_2D_pos);
                ManipulateHandMesh(aligned_palmCenter);
                UpdateMeshVisualizer(aligned_palmCenter);

                if (aligned_palmCenter)
                    action_detected_grasp = getAction_grasp();
            }
            
            else if(view_mode == VIEW_MODE.CAPE)
            {
                updateBinaryModule(true);
                hskl_error = getHandError();
                getBoneModelMatrices(hskl_poses);

                getFingerTipsFrom2D(ft2d_poss);
                getFingerTipsFromHSKL(hskl_poss);

                getFT2DTip(tip_FT2D_pos);
                getHSKLTip(tip_HSKL_pos);
                getTAPETip(tip_TAPE_pos);

                action_detected = getAction();
                bool aligned_fingertip = ManipulateFingertips();
                ManipulateHandMesh(aligned_fingertip);
                UpdateMeshVisualizer(aligned_fingertip);

                // get rotation & translation from binary module
            }
            else if (view_mode == VIEW_MODE.SD)
            {
                updateBinaryModule_SD(true);
                hskl_error = getHandError_SD();
                getBoneModelMatrices_SD(hskl_poses);

                UpdateSDGestureActionState(true);
                //action_detected_SDGesture = Input.GetKeyDown();
                //bool aligned_fingertip = ManipulateFingertips();
                ManipulateHandMesh(true);
                UpdateMeshVisualizer(true);

            }
     
            ManipulateTAPE();

		}
        if (ui_mode == UI_MODE.MARKER)
        {
           // UpdateARmarkerActionState();
            num_of_marker = runMarkerTracking();

            getMarkerPos(armarker_pos);
            getMarkerRotation(armarker_rotation);
            ManipulateMarker();

        }
	}
    public void UpdateSDGestureActionState(bool isKeyboardInput)
    {
		if (isKeyboardInput) 
		{
			//Debug.Log("key down");
			if (Input.GetKeyDown (KeyCode.LeftControl))
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.DOWN;
			else if (Input.GetKey (KeyCode.LeftControl))
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.STAY;
			else if (Input.GetKeyUp (KeyCode.LeftControl))
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.UP;
			else
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.IDLE;
		} 
		else 
		{
			int grabStatus = 0;

			getSDGesture (out currentStaticGestureID, out grabStatus);

			if (prevGrabStatus == 0 && grabStatus  == 1)
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.DOWN;
			else if (prevGrabStatus == 1 && grabStatus == 1)
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.STAY;
			else if (prevGrabStatus == 1 && grabStatus == 0)
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.UP;
			else if (prevGrabStatus == 0 && grabStatus == 0)
					action_detected_SDGesture = SerialConnector.BUTTON_STATE.IDLE;

			//Debug.Log(grabStatus);
			//Debug.Log(action_detected_SDGesture);
		}
	}

    public void UpdateARmarkerActionState()
    {
        if (ui_mode == UI_MODE.MARKER)
        {
            //after removing overlapping button State, assign real button state.
            
            SerialConnector.BUTTON_STATE overlappingDoubtful_ButtonState = mSerialConnector.getAction();
           // Debug.Log(overlappingDoubtful_ButtonState);
            if (overlappingDoubtful_ButtonState == SerialConnector.BUTTON_STATE.DOWN)
            {
                if (prevDownState == false)
                {
                    action_detected_ARmarker = SerialConnector.BUTTON_STATE.DOWN;
                    prevDownState = true;
                }
                else
                    action_detected_ARmarker = SerialConnector.BUTTON_STATE.STAY;
            }
            else if (overlappingDoubtful_ButtonState == SerialConnector.BUTTON_STATE.UP)
            {
                if (prevUpState == false)
                {
                    action_detected_ARmarker = SerialConnector.BUTTON_STATE.UP;
                    prevUpState = true;
                }
                else
                    action_detected_ARmarker = SerialConnector.BUTTON_STATE.IDLE;
            }
            else if (overlappingDoubtful_ButtonState == SerialConnector.BUTTON_STATE.STAY)
            {
                prevDownState = false;
                action_detected_ARmarker = overlappingDoubtful_ButtonState;
            }
            else
            {
                prevUpState = false;
                action_detected_ARmarker = overlappingDoubtful_ButtonState;
            }
            //Debug.Log(action_detected_ARmarker);
        }
    }
	public void UpdateByData(float error, float[] input_hskl_poses, float[] input_ft2d_poss, float[] input_hskl_poss,
	                         bool action, float[] input_tip_FT2D, float[] input_tip_HSKL, float[] input_tip_TAPE, float[] input_palmCenter,
	                         float[] input_wand_pos, float[] input_wand_rot, SerialConnector.BUTTON_STATE action_session2, int staticID)
	{
		hskl_error = error;

		hskl_poses = input_hskl_poses;
		ft2d_poss = input_ft2d_poss;
		hskl_poss = input_hskl_poss;

		tip_FT2D_pos = input_tip_FT2D;
		tip_HSKL_pos = input_tip_HSKL;
		tip_TAPE_pos = input_tip_TAPE;

		bool aligned = ManipulateFingertips();
		ManipulateHandMesh(aligned);
		ManipulateTAPE();

		currentStaticGestureID = staticID;
		action_detected_grasp = action;
		action_detected = action;

		if (view_mode == BinaryModuleImpl.VIEW_MODE.MARKER_UI)
			action_detected_ARmarker = action_session2;
		else if(view_mode == BinaryModuleImpl.VIEW_MODE.SD)
			action_detected_SDGesture = action_session2;

		palm_2D_pos = input_palmCenter;
		armarker_pos = input_wand_pos;
		armarker_rotation = input_wand_rot;
	
		ManipulateMarker();
		ManipulatePalmCenter2D ();

		/*
		if (ui_mode == UI_MODE.HAND)
		{
			hskl_error = error;

			hskl_poses = input_hskl_poses;
			ft2d_poss = input_ft2d_poss;
			hskl_poss = input_hskl_poss;

			action_detected_grasp = action;
			action_detected = action;
			currentStaticGestureID = staticID;
			action_detected_SDGesture = action_sdgesture;
			armarker_pos = input_wand_pos;
			armarker_rotation = input_wand_rot;
			action_detected_ARmarker = action_armarker;
			ManipulateMarker();

			if(view_mode == VIEW_MODE.GRAB)
			{
				action_detected_grasp = action;

			else if(view_mode == VIEW_MODE.CAPE)
				action_detected = action;

			else if(view_mode == VIEW_MODE.SD)
			{
				currentStaticGestureID = staticID;
				action_detected_SDGesture = action_sdgesture;
			}
		}
		
		if (ui_mode == UI_MODE.MARKER)
		{
            armarker_pos = input_wand_pos;
            armarker_rotation = input_wand_rot;
            action_detected_ARmarker = action_armarker;
			ManipulateMarker();
		}*/
	}

	public void OnDestroy()
	{

        if (view_mode == VIEW_MODE.GRAB || view_mode == VIEW_MODE.CAPE || view_mode == VIEW_MODE.MARKER_UI)
        {
            endMarkerTracking();
            endBinaryModule();
        }
        else if (view_mode == VIEW_MODE.SD)
        {
            endBinaryModule_SD();
        }
        //mSerialConnector.OnDestroy();
	}

	#endregion // MAIN_ROUTINE

	

	#region PRIVATE_METHODS_FOR_HSKL_HAMD

	private void InitHSKLHandMesh(GameObject hierarchy)
	{
		// copy vertices for mesh
		float[] hand_verts = new float[17*76*3*3];

        if(view_mode == VIEW_MODE.SD)
		    getHandMeshVertices_SD(hand_verts);
        else
            getHandMeshVertices(hand_verts);
		
		// create array of GameObject
		bones  = new GameObject[17];
		meshes = new Mesh[17];

		// 17 bones
		for(int id=0;id<17;id++){

			// create empty GameObject for each bone	
			string bone_name = "bone"+id;
			bones[id] = new GameObject(bone_name);
			
			// set transformation for this bone
			bones[id].transform.parent   = hierarchy.transform;
			bones[id].transform.position = hierarchy.transform.position;
			bones[id].transform.rotation = hierarchy.transform.rotation;
			bones[id].transform.localRotation = new Quaternion();
			
			// create mesh for this bone
			meshes[id] = new Mesh();
			meshes[id].name = "bonemesh"+id;
			
			// vertices for mesh
			meshes[id].vertices  = new Vector3[76*3];
			Vector3[] vertices = meshes[id].vertices;
			for(int j=0;j<76*3;j++){
				float x =  HAND_SIZE_SCALE * hand_verts[76 * 9 * id + 3 * j + 0];
				float y = -HAND_SIZE_SCALE * hand_verts[76 * 9 * id + 3 * j + 1]; // CAUTION: HSKL(x,y,z) -> Unity(x,-y,z)
				float z =  HAND_SIZE_SCALE * hand_verts[76 * 9 * id + 3 * j + 2];
				
				vertices[j] = new Vector3(x, y, z);
			}
			meshes[id].vertices = vertices;
			
			// triangles for mesh :: CAUTION - Unity mesh ordering
			meshes[id].triangles  = new int[76*3];
			int[] triangles = meshes[id].triangles;
			for(int j=0;j<76;j++){
				triangles[3*j+0] = 3*j+0;
				triangles[3*j+1] = 3*j+2; // CAUTION
				triangles[3*j+2] = 3*j+1; // CAUTION
			}
			meshes[id].triangles  = triangles;
			
			// uvs & normals for mesh
			meshes[id].uv = new Vector2[meshes[id].vertices.Length];
			meshes[id].normals = new Vector3[meshes[id].vertices.Length];
			
			// recalculate normals & bounds
			meshes[id].Optimize();
			meshes[id].RecalculateNormals();
			meshes[id].RecalculateBounds();
			
			// create MeshFilter for GameObject
			bones[id].AddComponent<MeshFilter>();
			MeshFilter meshFilter = bones[id].GetComponent<MeshFilter>();
			meshFilter.mesh = meshes[id];
			
			// create MeshRenderer for GameObject
			bones[id].AddComponent<MeshRenderer>();
			bones[id].renderer.material.color = Color.white;
            
            if(id == 1 || id == 14)
                bones[id].renderer.material.shader = Shader.Find("Transparent/Diffuse");
            
            
		}

		bones[0].renderer.enabled = false; // hide "wrist" bone
	}

	private void ManipulateHandMesh(bool aligned)
    {
        if (ui_mode != UI_MODE.HAND) return;

        Color mesh_color = (aligned) ? skinColor : failColor;
        
                // use palm (x,y,z) for size-free hand
        palm_pos.x = hskl_poses[7 * 1 + 4];
        palm_pos.y = hskl_poses[7 * 1 + 5];
        palm_pos.z = hskl_poses[7 * 1 + 6];

        // change transform at each bone
        for (int id = 0; id < 17; id++)
        {
            float rot_x = hskl_poses[7 * id + 0];
            float rot_y = hskl_poses[7 * id + 1];
            float rot_z = hskl_poses[7 * id + 2];
            float rot_w = hskl_poses[7 * id + 3];

            float pos_x = hskl_poses[7 * id + 4];
            float pos_y = hskl_poses[7 * id + 5];
            float pos_z = hskl_poses[7 * id + 6];

            bones[id].transform.localRotation = new Quaternion(-rot_x, rot_y, -rot_z, rot_w);
            bones[id].transform.localPosition = GetScaledPosition(pos_x, pos_y, pos_z);
            
            
            if((id == 1 || id == 14) && view_mode == VIEW_MODE.GRAB)
                bones[id].renderer.material.color = skinColor_transparent;
            else
                bones[id].renderer.material.color = mesh_color;
        }
    }

	#endregion // PRIVATE_METHODS_FOR_HSKL_HAMD



	#region PRIVATE_METHODS_FOR_FINGERTIPS

	private void InitFingertips(GameObject hierarchy)
	{
		// 5 fingertips (FT2D)
		tips_FT2D = new GameObject[5];
		for (int id = 0; id < 5; id++)
		{
			tips_FT2D[id] = CreateFingerTip(hierarchy.transform);
			tips_FT2D[id].name = "FingerTipFT2D" + id;
            tips_FT2D[id].renderer.material.shader = Shader.Find("Transparent/Diffuse");
			//tips_FT2D[id].renderer.material.color = (id == 4) ? failColor : skinColor_transparent ;
            tips_FT2D[id].renderer.material.color = skinColor_transparent;
			tips_FT2D[id].renderer.renderer.castShadows = false;
			tips_FT2D[id].collider.enabled = false;
            
		}

		// 5 fingertips (HSKL)
		tips_HSKL = new GameObject[5];
		for (int id = 0; id < 5; id++)
		{
			tips_HSKL[id] = CreateFingerTip(hierarchy.transform);
			tips_HSKL[id].name = "FingerTipHSKL" + id;
            tips_HSKL[id].renderer.material.color = Color.green;
			tips_HSKL[id].renderer.renderer.castShadows = false;
			tips_HSKL[id].renderer.enabled = false; // invisible
			tips_HSKL[id].collider.enabled = false;
		}
	}
    private void InitPalmCenter_DT(GameObject hierarchy)
    {
        palmCenter_2D = new GameObject();
        palmCenter_2D = CreateFingerTip(hierarchy.transform);
        palmCenter_2D.name = "PalmCenter2D";
        palmCenter_2D.renderer.material.shader = Shader.Find("Transparent/Diffuse");
        palmCenter_2D.renderer.material.color = new Color(1, 1, 0, 0.5f);
        palmCenter_2D.renderer.renderer.castShadows = false;
        palmCenter_2D.collider.enabled = false;

    }
	private bool ManipulateFingertips()
	{
		if (ui_mode != UI_MODE.HAND) return false;

		// FT2D tips
		for (int id = 0; id < 5; id++)
		{
			float pos_x = ft2d_poss[3 * id + 0];
			float pos_y = ft2d_poss[3 * id + 1];
			float pos_z = ft2d_poss[3 * id + 2];
            //Debug.Log(pos_x.ToString() + pos_y.ToString() + pos_z.ToString());
			tips_FT2D[id].transform.localPosition = GetScaledPosition(pos_x, pos_y, pos_z);
		}

		// HSKL tips
		for (int id = 0; id < 5; id++)
		{
			float pos_x = hskl_poss[3 * id + 0];
			float pos_y = hskl_poss[3 * id + 1];
			float pos_z = hskl_poss[3 * id + 2];
			tips_HSKL[id].transform.localPosition = GetScaledPosition(pos_x, pos_y, pos_z);
		}

		// tracking criteria
		int outlier = 0;
		for (int id = 0; id < 5; id++)
		{
			Vector3 ft2d_tip = tips_FT2D[id].transform.localPosition;
			Vector3 hskl_tip = tips_HSKL[id].transform.localPosition;

			Vector3 diff = ft2d_tip - hskl_tip;
			float sq_dist = Vector3.Dot(diff, diff);

			if( sq_dist > torelance*torelance ) outlier++;
		}

		if(outlier>=3) return false;
		return true;
	}
    private bool ManipulatePalmCenter2D()
    {
        bool aligned = false;
        if (ui_mode != UI_MODE.HAND) return false;

        float pos_x = palm_2D_pos[0];
        float pos_y = palm_2D_pos[1];
        float pos_z = palm_2D_pos[2];

        Vector3 palm_center_DT = GetScaledPosition(pos_x, pos_y, pos_z);
        Vector3 palm_center_HSKL = bones[1].transform.localPosition;

        //if (Vector3.Distance(palm_center_DT, palm_center_HSKL)) { }

        double dist = Vector3.Distance(palm_center_DT, palm_center_HSKL);
        
        Vector3 adjustedPos;
        if (dist > 80)
        {
            adjustedPos = palmCenter_DT_prev;
            aligned = false;
        }
        else
        {
            adjustedPos = palm_center_DT;
            palmCenter_DT_prev = adjustedPos;
            aligned = true;
        }
        palmCenter_2D.transform.localPosition = adjustedPos;

        return aligned;
    }
	#endregion // PRIVATE_METHODS_FOR_FINGERTIPS



	#region PRIVATE_METHODS_FOR_TAPE

	private void InitTAPE(GameObject for_dummy, GameObject hierarchy)
	{
		////////////////////////////////////////
		// for dummy **************************************** TEMPORARY...
		////////////////////////////////////////
		tip_FT2D_dummy = new GameObject("FingerTip_FT2D");
		tip_FT2D_dummy.transform.parent = for_dummy.transform;
		tip_FT2D_dummy.transform.position = for_dummy.transform.position;
		tip_FT2D_dummy.transform.rotation = for_dummy.transform.rotation;

		tip_HSKL_dummy = new GameObject("FingerTip_HSKL");
		tip_HSKL_dummy.transform.parent = for_dummy.transform;
		tip_HSKL_dummy.transform.position = for_dummy.transform.position;
		tip_HSKL_dummy.transform.rotation = for_dummy.transform.rotation;

		tip_TAPE_dummy = new GameObject("FingerTip_TAPE");
		tip_TAPE_dummy.transform.parent = for_dummy.transform;
		tip_TAPE_dummy.transform.position = for_dummy.transform.position;
		tip_TAPE_dummy.transform.rotation = for_dummy.transform.rotation;


		////////////////////////////////////////
		// for dummy **************************************** TEMPORARY...
		////////////////////////////////////////

		// 2D fingertip
		tip_FT2D = CreateFingerTip(hierarchy.transform);
		tip_FT2D.name = "FingerTip_FT2D";
		tip_FT2D.renderer.material.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
		tip_FT2D.renderer.material.shader = Shader.Find("Transparent/Diffuse");
		tip_FT2D.renderer.renderer.castShadows = false;
		tip_FT2D.collider.enabled = false;

		// HSKL fingertip
		tip_HSKL = CreateFingerTip(hierarchy.transform);
		tip_HSKL.name = "FingerTip_HSKL";
		tip_HSKL.renderer.material.color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
		tip_HSKL.renderer.material.shader = Shader.Find("Transparent/Diffuse");
		tip_HSKL.renderer.renderer.castShadows = false;
		tip_HSKL.collider.enabled = false;

		// CAPE fingertip (our)
		tip_TAPE = CreateFingerTip(hierarchy.transform);
		tip_TAPE.name = "FingerTip_TAPE";
		tip_TAPE.renderer.material.color = new Color(0.0f, 0.0f, 1.0f, 0.5f);
		tip_TAPE.renderer.material.shader = Shader.Find("Transparent/Diffuse");
		tip_TAPE.renderer.renderer.castShadows = false;
		tip_TAPE.collider.enabled = false;
	}

	private void ManipulateTAPE()
	{
		// CAPE (index finger tip only)
		if (action_detected && ui_mode == UI_MODE.HAND)
		{
			// calculate dummy
			tip_FT2D_dummy.transform.localPosition = GetScaledPosition(tip_FT2D_pos[0], tip_FT2D_pos[1], tip_FT2D_pos[2]);
			tip_HSKL_dummy.transform.localPosition = GetScaledPosition(tip_HSKL_pos[0], tip_HSKL_pos[1], tip_HSKL_pos[2]);
			tip_TAPE_dummy.transform.localPosition = GetScaledPosition(tip_TAPE_pos[0], tip_TAPE_pos[1], tip_TAPE_pos[2]);

			// insert position to real CAPE
			tip_FT2D.transform.position = tip_FT2D_dummy.transform.position;
			tip_HSKL.transform.position = tip_HSKL_dummy.transform.position;
			tip_TAPE.transform.position = tip_TAPE_dummy.transform.position;

			// enqueue for lazy reset
			while (visibleFrame.Count < RESET_FRAME)
			{
				bool dummy = true;
				visibleFrame.Enqueue(dummy);
			}
		}
	}

	#endregion // PRIVATE_METHODS_FOR_TAPE



	#region PRIVATE_METHODS_FOR_MESH_VISUALIZER

	private void InitMeshVisualizer(GameObject meshGameObject)
	{
		// add & set Mesh for visualization
		MeshFilter meshFilter = meshGameObject.GetComponent<MeshFilter>();

		meshForHand = new Mesh();
		meshForHand.name = "raw depth as mesh";
		
		// initial mesh vertices...
		meshForHand.vertices = new Vector3[ WIDTH * HEIGHT ];
		Vector3[] vertices = meshForHand.vertices;
		for (int j = 0; j < HEIGHT; j++)
		for (int i = 0; i < WIDTH ; i++)
		{
			float x = 1000.0f * (i - WIDTH /2);
			float y = 1000.0f * (j - HEIGHT/2);
			float z = 1000.0f;

			vertices[ i+j*WIDTH ] = new Vector3(x, y, z);
		}
		meshForHand.vertices = vertices;

		// initial mesh triangles...
		meshForHand.triangles = new int[ (WIDTH-1)*(HEIGHT-1)*6 ];
		int[] triangles = meshForHand.triangles;
		for (int j=0; j<HEIGHT-1; j++)
		for (int i=0; i<WIDTH -1; i++)
		{
			int idx = i + j * WIDTH;

			triangles[6*(i+j*(WIDTH-1))+0] = idx;
			triangles[6*(i+j*(WIDTH-1))+1] = idx + 1;
			triangles[6*(i+j*(WIDTH-1))+2] = idx + WIDTH;

			triangles[6*(i+j*(WIDTH-1))+3] = idx + 1;
			triangles[6*(i+j*(WIDTH-1))+4] = idx + WIDTH + 1;
			triangles[6*(i+j*(WIDTH-1))+5] = idx + WIDTH;
		}
		meshForHand.triangles = triangles;

		// uvs & normals for mesh
		meshForHand.uv = new Vector2[meshForHand.vertices.Length];
		meshForHand.normals = new Vector3[meshForHand.vertices.Length];

		// recalculate normals & bounds
		meshForHand.Optimize();
		meshForHand.RecalculateNormals();
		meshForHand.RecalculateBounds();
			
		// create MeshFilter for GameObject
		meshFilter.mesh = meshForHand;
	}

	private void UpdateMeshVisualizer(bool aligned)
	{
        if ( ui_mode != UI_MODE.HAND ) return; 

		// get point cloud information from binary module
		int size;
        IntPtr ptr;
        if(view_mode == VIEW_MODE.SD)
		    ptr = getPointCloud_SD(out size);
        else
            ptr = getPointCloud(out size);

		if(size != 0) Marshal.Copy(ptr, floatArray, 0, size * 3);

		// initial mesh vertices...
		meshForHand.vertices = new Vector3[WIDTH * HEIGHT];
		Vector3[] vertices = meshForHand.vertices;
		for (int j=0; j<HEIGHT; j++)
		for (int i=0; i<WIDTH ; i++)
		{
			float x = 1000.0f * (floatArray[3 * (i + j * WIDTH) + 0]);
			float y =-1000.0f * (floatArray[3 * (i + j * WIDTH) + 1]);
			float z = 1000.0f * (floatArray[3 * (i + j * WIDTH) + 2]) - z_offset_pointcloud;

			vertices[i + j * WIDTH] = new Vector3(x, y, z);
		}
		meshForHand.vertices = vertices;

		// recalculate normals & bounds
		meshForHand.RecalculateNormals();
		
		// set mesh color
		mMeshVisualizer.renderer.material.color = (aligned) ? skinColor : failColor;
	}

	#endregion // PRIVATE_METHODS_FOR_MESH_VISUALIZER
	#region PRIVATE_METHODS_FOR_ARTOOLKIT


	private void InitARWandCuror(GameObject hierarchy)
	{
        initMarkerTracking();
		
		// AR cursor
		cursor_arwand = GameObject.Instantiate(Resources.Load("ARWand2_new") as GameObject, hierarchy.transform.position, hierarchy.transform.rotation) as GameObject;
        cursor_arwand.name = "cursor_arwand";
        cursor_arwand.transform.parent = hierarchy.transform;
        cursor_arwand.transform.position = hierarchy.transform.position;
		cursor_arwand.transform.rotation = hierarchy.transform.rotation;
	}

	private void InitSDCursor(GameObject hierarchy)
	{
		// AR cursor
		cursor_sd= GameObject.Instantiate(Resources.Load("SDGesture") as GameObject, hierarchy.transform.position, hierarchy.transform.rotation) as GameObject;
		cursor_sd.name = "cursor_sd";
		cursor_sd.transform.parent = hierarchy.transform;
		cursor_sd.transform.localPosition = new Vector3(16.92036f, 90.98541f, 93.62593f);
		cursor_sd.transform.localRotation = Quaternion.Euler(273.9909f, 120.6126f, 71.83623f);
	}
	
	
	
	private void ManipulateMarker()
	{
		if (ui_mode != UI_MODE.MARKER)
		{
//			cursor_arwand.renderer.enabled   = false;
			return;
		}
		//if (num_of_marker > 0)
		{
			float x = armarker_pos[0];
			float y = armarker_pos[1];
			float z = armarker_pos[2];

			Vector3 pos = new Vector3(x, y, z);
            //Debug.Log(pos);
            cursor_arwand.transform.localPosition = pos;
			cursor_arwand.transform.localRotation = new Quaternion(armarker_rotation[0], armarker_rotation[1], armarker_rotation[2], armarker_rotation[3]);
		}
		//else
		{
			// set invisible
			//cursor_arwand.renderer.enabled = false;
		}
	}

	#endregion // PRIVATE_METHODS_FOR_ARTOOLKIT
	


	#region PRIVATE_METHODS_SUBROUTINE

	private GameObject CreateFingerTip(Transform tr_parent) 
	{
		GameObject tip = GameObject.CreatePrimitive(PrimitiveType.Cube);
		tip.transform.parent = tr_parent;
		tip.transform.position = tr_parent.position;
		tip.transform.rotation = tr_parent.rotation;
		tip.transform.localRotation = Quaternion.Euler(45, 45, 45);
		tip.transform.localScale = FingertipSize;

		return tip;
	}

	private Vector3 GetScaledPosition(float pos_x, float pos_y, float pos_z)
	{
		Vector3 vec = Vector3.zero;
		vec.x = (TRANSLATE_SCALE * palm_pos.x + HAND_SIZE_SCALE * (pos_x - palm_pos.x));
		vec.y =-(TRANSLATE_SCALE * palm_pos.y + HAND_SIZE_SCALE * (pos_y - palm_pos.y));
		vec.z = (TRANSLATE_SCALE * palm_pos.z + HAND_SIZE_SCALE * (pos_z - palm_pos.z));

		return vec;
	}

	#endregion // PRIVATE_METHODS_SUBROUTINE
}
