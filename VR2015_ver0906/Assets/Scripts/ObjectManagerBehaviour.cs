﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class ObjectManagerBehaviour : MonoBehaviour{

	#region CONSTANTS_FOR_USERTEST

	const int REVERSE_FRAME = 180; // reverse the velocity : 180/60 = 3 [sec]

	const int RESET_FRAME = 30; // reset color after RESET_FRAME : 30/60 = 0.5 [sec]

	float CubeSize = 20.0f; // virtual object size = 20[mm]

	Color selectColor;

    public const float SIZE_PALM = 120;
	public const float SIZE_LARGE = 40;
	public const float SIZE_MEDIUM = 20;
	public const float SIZE_SMALL = 10;


	#endregion // CONSTANTS_FOR_USERTEST



	#region PRIVATE_MEMBERS

	private Vector3 cube_scale;

	private GameObject hierarchy;

	private GameObject[] cubeObjects = null;

	private Queue<int> selectedIDs = new Queue<int>();

	private bool rotation = false;
	private float rotation_vel;

	private bool velocity = false;
	private int reverse_frame;


	#endregion // PRIVATE_MEMBERS




	#region MONO_BEHAVIOUR_METHODS

	void Start()
	{
		selectColor = new Color(0.4f, 0.0f, 0.6f);

		// get scale for positioning the random cubes...
		cube_scale = new Vector3(CubeSize, CubeSize, CubeSize);

		// hierarchy for off-centered rotation
		hierarchy = new GameObject("center");
		hierarchy.transform.parent = this.transform;
	}

	public void FixedUpdate()
	{
		// reset object color
		if (selectedIDs.Count > 0)
		{
			int id = selectedIDs.Dequeue();
			if (id != -1)
			{
                if(id == 0)
                    cubeObjects[id].renderer.material.color = Color.magenta; // default as CYAN
                else
                    cubeObjects[id].renderer.material.color = Color.cyan; // default as CYAN

				cubeObjects[id].transform.localScale = cube_scale;
			}
		}

		// off-centered rotation
		if (rotation) hierarchy.transform.Rotate(0, rotation_vel, 0);

		// velocity reverse to avoid "out-of-bound"
		if (velocity)
		{
			reverse_frame++;
			if (reverse_frame % REVERSE_FRAME == 0)
			{
				for (int id = 0; id < cubeObjects.Length; id++)
				{
					cubeObjects[id].rigidbody.velocity *= -1;
				}
			}
		}
	}

	#endregion // MONO_BEHAVIOUR_METHODS



	#region PUBLIC_METHODS_FOR_USERVIEW
    public void ChangeObjectColorWithID(int id)
    {
        // selected color as GRAY
        cubeObjects[id].renderer.material.color = selectColor;
        cubeObjects[id].transform.localScale = 1.5f * cube_scale;

        // enqueue for lazy reset
        while (selectedIDs.Count < RESET_FRAME)
        {
            int dummy = -1;
            selectedIDs.Enqueue(dummy);
        }
        selectedIDs.Enqueue(id);
    }
	public void ChangeObjectColorWithID(int id, Color selectColor)
	{
		// selected color as GRAY
		cubeObjects[id].renderer.material.color = selectColor;
		cubeObjects[id].transform.localScale = 1.5f*cube_scale;

		// enqueue for lazy reset
		while (selectedIDs.Count < RESET_FRAME)
		{
			int dummy = -1;
			selectedIDs.Enqueue(dummy);
		}
		selectedIDs.Enqueue(id);
	}

    public void ChangeObjectSpecificColorWithID(int id, Color selectColor)
    {
        // selected color as GRAY
       	cubeObjects[id].renderer.material.color = selectColor;
        //cubeObjects[id].transform.localScale = 1.5f * cube_scale;
    }


	#endregion // PUBLIC_METHODS_FOR_USERVIEW



	#region PUBLIC_METHODS_FOR_MEASUREMENT

	public void GetClosestObjectToPoint(Vector3 pos, out int minID, out float error, out Vector3 minObj)
	{
		// initialize "out" data
		minObj = Vector3.zero;
		minID = -1;

		float minErrSq = float.MaxValue;
		for (int id = 0; id < cubeObjects.Length; id++)
		{
			Vector3 obj = cubeObjects[id].transform.position;

			Vector3 dist = pos - obj;
			float errSq = Vector3.Dot(dist, dist);
			if (errSq < minErrSq)
			{
				minID = id;
				minErrSq = errSq;
				minObj = obj;
			}
		}

		// get root from Dist^2
		error = Mathf.Sqrt(minErrSq);
	}
    public void GetTargetObjectToPoint(Vector3 pos, out float error)
    {
        error = Vector3.Distance(pos, cubeObjects[0].transform.position);
    }

	public void GetClosestObjectToRay(Vector3 pos, Vector3 dir, out int minID, out float error, out Vector3 minObj)
	{
		// initialize "out" data
		minObj = Vector3.zero;
		minID = -1;

		float minErrSq = float.MaxValue;
		for (int id = 0; id < cubeObjects.Length; id++)
		{
			Vector3 obj = cubeObjects[id].transform.position;

			// get cross-section of ray and plane
			float Dir_dot_Pos0 = Vector3.Dot(dir, pos);
			float Dir_dot_Pos1 = Vector3.Dot(dir, obj);
			float Dir_dot_Dir = Vector3.Dot(dir, dir);
			float param_t = (Dir_dot_Pos1 - Dir_dot_Pos0) / Dir_dot_Dir;
			Vector3 crossPt = pos + param_t * dir;

			Vector3 dist = crossPt - obj;
			float errSq = Vector3.Dot(dist, dist);
			if (errSq < minErrSq)
			{
				minID = id;
				minErrSq = errSq;
				minObj = obj;
			}
		}

		// get root from Dist^2
		error = Mathf.Sqrt(minErrSq);
	}

	public GameObject GetObjectByID(int ID)
	{
		return cubeObjects[ID];
	}

	#endregion // PUBLIC_METHODS_FOR_MEASUREMENT



	#region PUBLIC_METHODS_FOR_BLOB_ARRANGEMENT

	public void DeleteObjects()
	{
		// clear the temporal data
		selectedIDs.Clear();
		velocity = false;
		rotation = false;

		// delete all objects
		if (cubeObjects != null)
		{
			int count = cubeObjects.Length;
			for (int i = 0; i < count; i++)
			{
				Destroy((Object)cubeObjects[i]);
			}
		}
	}

	public void Training(int width, int height)
	{
		const float safety_x = 75f;
		const float safety_y = 75f;


		float step_x = (180.0f - 2.0f * safety_x)/ (width + 1);
        float step_y = (180.0f - 2.0f * safety_y)/ (height + 1);

		DeleteObjects();

		// set index list
		cubeObjects = new GameObject[width * height];

		////////////////////////////////////////////////////////////
		// set positions in the grid
		////////////////////////////////////////////////////////////
		int id = 0;
		for (int j = 1; j <= height; j++)
		for (int i = 1; i <= width; i++)
		{
			// get index
			float radius = 400.0f;
			float theta = Mathf.Deg2Rad * (safety_x + step_x * i);
			float phi = Mathf.Deg2Rad * (safety_y + step_y * j);

			float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
			float y = radius * Mathf.Cos(phi);
			float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

			// 
			SetSingleCubePosition(id, x, y, z);
			id++;
		}
	}


	// scenario #0: "freeplay" --- 8x8 is the best...
	public void Freeplay(int width, int height)
	{
        float safety_x = 60.0f;
        float safety_y = 70.0f;

		// 
        float step_x = (180.0f - 2.0f * safety_x)  / (width + 1);
        float step_y = (180.0f - 2.0f * safety_y)  / (height + 1);

		DeleteObjects();

		// set index list
		cubeObjects = new GameObject[width * height];

		////////////////////////////////////////////////////////////
		// set positions in the grid
		////////////////////////////////////////////////////////////
		int id = 0;
		for (int j = 1; j <= height; j++)
		for (int i = 1; i <= width; i++)
		{
			// get index
			float radius = 400.0f;
			float theta = Mathf.Deg2Rad * (safety_x + step_x * i);
			float phi = Mathf.Deg2Rad * (safety_y + step_y * j);

			float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
			float y = radius * Mathf.Cos(phi);
			float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

			// 
			SetSingleCubePosition(id, x, y, z);
			id++;
		}
	}

	// scenario #1 : "sparse"
	public void Scenario1(int horizontal, int vertical, int random_seed, int numOfSpheres, bool is_dynamic, float velocityMagnitude, int method)
	{
        const float safety_x = 0;
        const float safety_y = 30.0f;

		// check & delete spheres ...
		DeleteObjects();

		// set random seed
		Random.seed = random_seed;

		// generate sphere at random position
		cubeObjects = new GameObject[numOfSpheres];

        float radius_min = 240;
        float radius_max = 450.0f;

		Debug.Log (method);
        if (method == 0)
        {
            radius_min = 240;
            radius_max = 350.0f;
        }
       
		// id #0
		{
			// set random blob in hemi-sphere
            float radius = Random.Range(radius_min + CubeSize, radius_max - CubeSize);
			float theta = Mathf.Deg2Rad * Random.Range(72.0f+54.0f*horizontal, 108.0f+54.0f*horizontal);
			float phi = Mathf.Deg2Rad * Random.Range(78.0f+36.0f*vertical, 102.0f+36.0f*vertical);

			float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
			float y = radius * Mathf.Cos(phi);
			float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

			SetSingleCubePosition(0, x, y, z);
		}

		// id #1 ~ #{numOfSpheres-1}
		for (int id = 1; id < numOfSpheres; id++)
		{
			// set random position
			while (true)
			{
				// set random blob in hemi-sphere
                float radius = Random.Range(radius_max + CubeSize, radius_max - CubeSize);
				float theta = Mathf.Deg2Rad * Random.Range(safety_x, 180.0f - safety_x); // [0:180] degree with safety area
				float phi = Mathf.Deg2Rad * Random.Range(safety_y, 180.0f - safety_y); // [0:360] degree with safety area

				float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
				float y = radius * Mathf.Cos(phi);
				float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

				// minimum distance from other objects
				Vector3 pos = new Vector3(x, y, z);
				bool aligned = true;
				for (int n = 0; n < id; n++)
				{
					Vector3 pt = cubeObjects[n].transform.position;
					Vector3 diff = pos - pt;
					float distSq = Vector3.Dot(diff, diff);
					if (distSq < CubeSize * CubeSize * 3.0f)
					{
						aligned = false;
						break;
					}
				}

				if (aligned)
				{
					SetSingleCubePosition(id, x, y, z);
					break;
				}
			}
		}
		cubeObjects[0].renderer.material.color = Color.magenta; // selection object as RED

		// set vecloity
		if (is_dynamic)
		{
			velocity = is_dynamic;
			reverse_frame = REVERSE_FRAME / 2;
			SetAllCubeWithDynamics(velocityMagnitude);
		}
	}

	// scenario #2: "dense"
    public void Scenario2(int horizontal, int vertical, int random_seed, int width, int height, bool is_dynamic, int method)
	{
		DeleteObjects();

		// set hierarchy for rotation with grid formation
        int z_distance = 350;
        if (method == 0) z_distance = 300;

        hierarchy.transform.position = new Vector3(150 * horizontal, 150 * vertical, z_distance);
		hierarchy.transform.rotation = Quaternion.Euler(90.0f - 45 * vertical, 45 *horizontal , 0);

		// set random seed
		Random.seed = random_seed;

		// set index list
		cubeObjects = new GameObject[width * height];

		////////////////////////////////////////////////////////////
		// set positions in the grid
		////////////////////////////////////////////////////////////
		for (int j = 0; j < height; j++)
		for (int i = 0; i < width; i++)
		{
			// get index
			float x = (i - (width) / 2 + 0.5f) * 30.0f * CubeSize/20;
			float y = 0;
			float z = (j - (height) / 2 + 0.5f) * 30.0f* CubeSize/20;

			// 
			SetSingleCubePosition(i + j * width, x, y, z);
			cubeObjects[i + j * width].transform.parent = hierarchy.transform;
			cubeObjects[i + j * width].transform.localPosition = new Vector3(x, y, z);
		}

		// swap positions
		int idx_i = Random.Range(2, width-2);
		int idx_j = Random.Range(2, height-2);
		int id = idx_i + idx_j * width;
		Vector3 pos = cubeObjects[0].transform.position;
		cubeObjects[0].transform.position = cubeObjects[id].transform.position;
		cubeObjects[id].transform.position = pos;

		// selection object as RED
		cubeObjects[0].renderer.material.color = Color.magenta;

		// set dynamics
		rotation = is_dynamic;
		rotation_vel = 0.5f;
	}

	// scenario #3
	public void Scenario3(int random_seed, int width, int height)
	{
		DeleteObjects();

		// set random seed
		Random.seed = random_seed;

		// set index list
		cubeObjects = new GameObject[width * height];

		////////////////////////////////////////////////////////////
		// set positions in the grid
		////////////////////////////////////////////////////////////
		for (int j = 0; j < height; j++)
		for (int i = 0; i < width ; i++)
		{
			// get index
			float x = (i - (width) / 2 + 0.5f) * 30.0f* CubeSize/20;
			float y = (j - (height) / 2 + 0.5f) * 30.0f* CubeSize/20;
			float z = 350;

			// 
			SetSingleCubePosition(i+j*width, x, y, z);
		}

		// swap positions
		int idx_i = Random.Range(1, width - 1);
		int idx_j = Random.Range(1, height - 1);
		int id = idx_i + idx_j * width;
		Vector3 pos = cubeObjects[0].transform.position;
		cubeObjects[0].transform.position = cubeObjects[id].transform.position;
		cubeObjects[id].transform.position = pos;

		cubeObjects[0].renderer.material.color = Color.magenta; // selection object as RED
	}

	public void Scenario_Session2(int horizontal, int vertical, int random_seed, int numOfSpheres, bool is_dynamic, float velocityMagnitude)
    {
        const float safety_x = 50.0f;
        const float safety_y = 75.0f;

        // check & delete spheres ...
        DeleteObjects();

        // set random seed
        Random.seed = random_seed+1;

        // generate sphere at random position
        cubeObjects = new GameObject[2];

        float radius_min = 270;
        float radius_max = 320.0f;

        // create source object.
        float radius = Random.Range(radius_min + CubeSize, radius_max - CubeSize);
        float theta = Mathf.Deg2Rad * Random.Range(safety_x, 180.0f - safety_x); // [0:180] degree with safety area
        float phi = Mathf.Deg2Rad * Random.Range(safety_y, 180.0f - safety_y); // [0:360] degree with safety area

        Vector3 source_direction = new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        Vector3 target_direction = new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));


        float x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
        float y = radius * Mathf.Cos(phi);
        float z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

        SetSingleCubePosition_session2(0, x, y, z, source_direction, true);



        // create target object.
        radius = Random.Range(radius_max + CubeSize, radius_max - CubeSize);
        theta = Mathf.Deg2Rad * Random.Range(safety_x, 180.0f - safety_x); // [0:180] degree with safety area
        phi = Mathf.Deg2Rad * Random.Range(safety_y, 180.0f - safety_y); // [0:360] degree with safety area

        
        x = radius * Mathf.Sin(phi) * Mathf.Cos(theta);
        y = radius * Mathf.Cos(phi);
        z = radius * Mathf.Sin(phi) * Mathf.Sin(theta);

        SetSingleCubePosition_session2(1, x, y, z, target_direction, false);


        // set vecloity
        if (is_dynamic)
        {
            velocity = is_dynamic;
            reverse_frame = REVERSE_FRAME / 2;
            SetAllCubeWithDynamics(velocityMagnitude);

        }

      
    }
 
	#endregion // PUBLIC_METHODS_FOR_BLOB_ARRANGEMENT



	#region SUBROUTINES

	private void SetSingleCubePosition(int id, float x, float y, float z)
	{
		cubeObjects[id] = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cubeObjects[id].name = "Object" + id;
		cubeObjects[id].transform.parent = this.transform;
		cubeObjects[id].transform.localScale = cube_scale;
		cubeObjects [id].transform.tag = "object";

		// cube position by spherical coordinate
		cubeObjects[id].transform.position = new Vector3(x, y, z);

		// default color as CYAN
		cubeObjects[id].renderer.material.color = Color.cyan;

		// add Rigidbody for bouncing
		cubeObjects[id].AddComponent<Rigidbody>();
		Rigidbody rigidbody = cubeObjects[id].GetComponent<Rigidbody>();
		rigidbody.mass = 1.0f;
		rigidbody.drag = 0.0f;
		rigidbody.angularDrag = 0.0f;
		rigidbody.useGravity = false;
		rigidbody.angularVelocity = new Vector3(1, 1, 1); // for default rotation
		rigidbody.velocity = Vector3.zero;

		// add SphereCollider (elastic collision)
		BoxCollider collider = cubeObjects[id].GetComponent<BoxCollider>();

		////////////////////////////////////////////////////////////
		/// iJeon modified 150804
		// collider.isTrigger = true;
		/// collider.enabled = false;
		////////////////////////////////////////////////////////////
        collider.isTrigger = true;
		collider.enabled = true;
	}
    private void SetSingleCubePosition_session2(int id, float x, float y, float z, Vector3 direction, bool isSource)
    {
        
        //cubeObjects[id] = GameObject.CreatePrimitive(PrimitiveType.Cube);
        if(isSource)
            cubeObjects[id] = GameObject.Instantiate(Resources.Load("source_new"), transform.position, transform.rotation) as GameObject;
        else
            cubeObjects[id] = GameObject.Instantiate(Resources.Load("target_new"), transform.position, transform.rotation) as GameObject;

        cubeObjects[id].name = "Object" + id;
        cubeObjects[id].transform.parent = this.transform;
        cubeObjects[id].transform.localScale = new Vector3(cube_scale.x, cube_scale.y, cube_scale.z);
        
        cubeObjects[id].transform.tag = "object";
        /*Vector3 newDir = Vector3.RotateTowards(cubeObjects[id].transform.forward, direction, 1, 0);*/
        //Debug.Log(direction);
        cubeObjects[id].transform.localRotation = Quaternion.LookRotation(direction);

        // cube position by spherical coordinate
        cubeObjects[id].transform.position = new Vector3(x, y, z);

        /*
        // default color as CYAN
        cubeObjects[id].renderer.material.color = Color.gray;
        
        // add Rigidbody for bouncing
        cubeObjects[id].AddComponent<Rigidbody>();
        Rigidbody rigidbody = cubeObjects[id].GetComponent<Rigidbody>();
        rigidbody.mass = 1.0f;
        rigidbody.drag = 0.0f;
        rigidbody.angularDrag = 0.0f;
        rigidbody.useGravity = false;
        //rigidbody.angularVelocity = new Vector3(1, 1, 1); // for default rotation
        rigidbody.velocity = Vector3.zero;

        // add SphereCollider (elastic collision)
        BoxCollider collider = cubeObjects[id].GetComponent<BoxCollider>();

        ////////////////////////////////////////////////////////////
        /// iJeon modified 150804
        // collider.isTrigger = true;
        /// collider.enabled = false;
        ////////////////////////////////////////////////////////////
        collider.isTrigger = true;
        collider.enabled = true;
        */
         
    }
	private void SetAllCubeWithDynamics(float velocityMagnitude)
	{
		////////////////////////////////////////////////////////////
		// set dynamics : velocity and elastic collision
		////////////////////////////////////////////////////////////
		for (int i = 0; i < cubeObjects.Length; i++)
		{
			// set velocity
			Vector3 vel = new Vector3();
			vel.x = Random.Range(-1.0f, 1.0f);
			vel.y = Random.Range(-1.0f, 1.0f);
			vel.z = Random.Range(-1.0f, 1.0f);
			vel.Normalize();
			vel *= velocityMagnitude;

			// Rigidbody (for velocity)
			Rigidbody rigidbody = cubeObjects[i].GetComponent<Rigidbody>();
			rigidbody.velocity = vel;
		}
	}
    public void SetCubeSize(float size, int session_num)
	{

		CubeSize = size;

        if(session_num == 1)
        {
			cube_scale = new Vector3(CubeSize, CubeSize, CubeSize);
        }
        else if (session_num == 2)
        {
			cube_scale = new Vector3(CubeSize*2, CubeSize, CubeSize);
            Debug.Log(cube_scale);
        }
	}
	#endregion // SUBROUTINES
}
