﻿Shader "Custom/Particle Alpha Blending Shadow" {
Properties {
	
}

// 2/3 texture stage GPUs
SubShader {
	Tags {"IgnoreProjector"="True" "Queue"="Geometry+1"  "RenderType"="TransparentCutout"}
	LOD 100
	
	// Pass to render object as a shadow caster
	Pass {
		Name "Caster"
		Tags { "LightMode" = "ShadowCaster" }
		Offset 1, 1
		
		Fog {Mode Off}
		ZWrite On ZTest Less Cull Off

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct v2f { 
				V2F_SHADOW_CASTER;
				float2  uv : TEXCOORD1;
			};

			uniform float4 _MainTex_ST;

			v2f vert( appdata_base v )
			{
				v2f o;
				TRANSFER_SHADOW_CASTER(o)
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}

			uniform sampler2D _MainTex;
			uniform float _Cutoff;
			uniform float4 _Color;

			float4 frag( v2f i ) : COLOR
			{
				half4 texcol = tex2D( _MainTex, i.uv );
				clip( texcol.a*_Color.a - _Cutoff );
	
				SHADOW_CASTER_FRAGMENT(i)
			}
		ENDCG
	}
	
	// Pass to render object as a shadow collector
	Pass {
		Name "ShadowCollector"
		Tags { "LightMode" = "ShadowCollector" }
		
		Fog {Mode Off}
		ZWrite On
		ZTest Less
		ColorMask 0 
	

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_shadowcollector

			#define SHADOW_COLLECTOR_PASS
			#include "UnityCG.cginc"

			struct v2f {
			V2F_SHADOW_COLLECTOR;
			float2  uv : TEXCOORD5;
			};

			uniform float4 _MainTex_ST;

			v2f vert (appdata_base v)
			{
			v2f o;
			TRANSFER_SHADOW_COLLECTOR(o)
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			return o;
			}

			uniform sampler2D _MainTex;
			uniform float _Cutoff;
			uniform float4 _Color;

			half4 frag (v2f i) : COLOR
			{
			half4 texcol = tex2D( _MainTex, i.uv );
			clip( texcol.a*_Color.a - _Cutoff );
	
			SHADOW_COLLECTOR_FRAGMENT(i)
			}
		ENDCG
	}

	// Pass to render object
	Pass {
		
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_particles
			
		#include "UnityCG.cginc"

		sampler2D _MainTex;
			
		struct appdata_t {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
		};

		struct v2f {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float2 texcoord : TEXCOORD0;
		};
			
		float4 _MainTex_ST; // must be!

		v2f vert (appdata_t v)
		{
			v2f o;
			o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
			o.color = v.color;
			o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
			return o;
		}

		fixed4 frag (v2f i) : COLOR
		{
			return i.color;
		}
		ENDCG 
	}


}

}
